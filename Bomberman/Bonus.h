/////////////////////////////////////////////////////////////
//														   //
//		/*PROJET BOMBERMAN - MTI 3D - */				   //
//														   //
//				----- Class Bonus -----		     		   //
//														   //
/////////////////////////////////////////////////////////////

#pragma once
#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>
#include <windows.h> 
#include "SOIL/SOIL.h"
#include "GL\glut.h"

class Bonus
{

public :

	float vitesse;
	int nbBombe;
	int distance;
	int vie;
	

public:

	void UpdateVitesse();
	void UpdateNbBombe();
	void UpdateDistance();
	void UpdateVie();

	int ReturnDistance();
	int ReturnVitesse();
	int ReturnNbBombe();

	void Reset();

	Bonus();
	
};

