/////////////////////////////////////////////////////////////
//														   //
//		/*PROJET BOMBERMAN - MTI 3D - */                   //
//														   //
//				----- Class Ennemi -----				   //
//														   //
/////////////////////////////////////////////////////////////



class ennemi 
{

public : 
	float offsetX;
	float offsetY;

	int x;
	int y;

	int nbVies;
	int img;

	bool depH;
	bool depB;
	bool depG;
	bool depD;

	bool touch;
	

public:

	ennemi();

	void UpdateVie();
	int ReturnVie();
	void ResetVie(int);

	void ResetPos(int, int,int);
	int PositionX();
	int PositionY();
	

	void DepDroite(int);
	void DepGauche(int);
	void DepTop(int);
	void DepBottom(int);

	void Dessiner();
	

	//Fonction pour g�rer al�atoirement le d�plcament de l'ennemi//
	void DeplacementEnnemi(int, int, int, int);

	

	
};



