/////////////////////////////////////////////////////////////
//														   //
//		/*PROJET Bomberman- MTI 3D - */                    //
//														   //
//				----- Class Avatar -----				   //
//														   //
/////////////////////////////////////////////////////////////


class avatar 
{

public:
	bool key;
	int nbBombe;
	int _img;
	float offsetX;
	float offsetY;
	float vitesse;

	int x;
	int y;
	int nbVies;

	bool touch;

	bool depD;
	bool depG;
	bool depH;
	bool depB;

public:
	
	avatar(int, int,int);

	int ReturnVitesse();
	int NbBombe();
	

	int PositionX();
	int PositionY();
	void ResetPos(int, int);

	void UpdateVie();
	int ReturnVie();



	//Fonctions de déplacements de l'avatar//
	void DepGauche();
	void DepDroite();
	void DepTop();
	void DepBottom();

	//Fonction pour afficher l'avatar//
	void Dessiner();

	bool ReturnKey();

	void Reset(int, int, int);

};


