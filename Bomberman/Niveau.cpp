/////////////////////////////////////////////////////////////
//														   //
//		/*PROJET BOMBERMAN - MTI 3D - */				   //
//														   //
//				----- Class Niveau -----				   //
//														   //
/////////////////////////////////////////////////////////////

#include "Niveau.h"
#include "time.h"
#include "avatar.h"

using namespace std;

extern ifstream fichier;
extern int LARGEUR;
extern int HAUTEUR;
extern char** Matrice;

extern vector<GLuint> texture; 
extern avatar player;

Niveau::Niveau()
{
	//Matrice a l'envers dans le fichier .txt
	NbLignes = 11;
	NbColonnes = 11;
	niveau = 1;
	
	keyX = 0;
	keyY = 0;
	
}

void Niveau::Texture(int _img)
{
	img = _img;
}

void Niveau::OuvrirNiveau(char* nom_fichier)
{
	ifstream fichier; // Objet de type ifstream
	fichier.open(nom_fichier); // Ouverture du fichier

	if (!fichier) { // Test de l'existence du fichier
		cout << "Erreur lors de l'ouverture du fichier !"
			<< endl;
		system("pause");
		exit(1);
	}
	

	// Lecture de la taille du niveau
	//fichier >> NbLignes;
	//fichier >> NbColonnes;
	

	// Allocation du tableau du niveau
	Matrice = new char*[NbLignes];

	for (int i = 0; i<NbLignes; i++)
		Matrice[i] = new char[NbColonnes];

	// Initialisation des valeurs du tableau
	for (int i = 0; i<NbLignes; i++)
		for (int j = 0; j<NbColonnes; j++)
			Matrice[j][i] = '0';

	// Lecture du tableau du niveau, caract�re par caract�re
	for (int j = 0; j < NbColonnes; j++) {
		for (int i = 0; i < NbLignes; i++) {
			
			fichier >> Matrice[j][i];

			
		}
		
	}


	fichier.close(); // Fermeture du fichier
}

void Niveau::DessinerNiveau()
{
	//AFFICHAGE DU NIVEAU DE JEU //

	glViewport(LARGEUR / 4, HAUTEUR / 10, LARGEUR*0.5, HAUTEUR*0.8);
	glLoadIdentity();


	////Affichage de la map /////

	//int i = 0, j = 0;
	for (int i = 0; i < NbLignes; i++)
	{
		for (int j = 0; j < NbColonnes; j++)
		{
			
			switch (Matrice[j][i])
			{
				
			case '0': // couloir
				glPushMatrix();
				
				glEnable(GL_BLEND);
				glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
				
				glBegin(GL_QUADS);
				glColor4d(1.0, 1.0, 1.0, 0.0);
				glTexCoord2f(0.0f, 1.0f); glVertex2d(i, j);
				glTexCoord2f(1.0f, 1.0f); glVertex2d(i + 1, j);
				glTexCoord2f(1.0f, 0.0f); glVertex2d(i + 1, j + 1);
				glTexCoord2f(0.0f, 0.0f); glVertex2d(i, j + 1);
				glEnd();
				
				glPopMatrix();
				glutPostRedisplay();
				break;

			case '1': // mur
				int tex;
				if (niveau == 1) {
					tex = 0;
				}
				else if (niveau == 2) {
					tex = 1;
				}
				else if (niveau == 3) {
					tex = 97;
				}
				glPushMatrix();
				glEnable(GL_TEXTURE_2D);
				glEnable(GL_BLEND);
				glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
				glBindTexture(GL_TEXTURE_2D, texture[tex]);
				glBegin(GL_QUADS);
				glColor4d(1.0, 1.0, 1.0, 1.0);
				glTexCoord2f(0.0f, 1.0f); glVertex2d(i, j);
				glTexCoord2f(1.0f, 1.0f); glVertex2d(i + 1, j);
				glTexCoord2f(1.0f, 0.0f); glVertex2d(i + 1, j + 1);
				glTexCoord2f(0.0f, 0.0f); glVertex2d(i, j + 1);
				glEnd();
				glDisable(GL_TEXTURE_2D);
				glPopMatrix();
				glutPostRedisplay();
				break;

			case '�': // mur Level 3
				
				glPushMatrix();
				glEnable(GL_TEXTURE_2D);
				glEnable(GL_BLEND);
				glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
				glBindTexture(GL_TEXTURE_2D, texture[95]);
				glBegin(GL_QUADS);
				glColor4d(1.0, 1.0, 1.0, 1.0);
				glTexCoord2f(0.0f, 1.0f); glVertex2d(i, j);
				glTexCoord2f(1.0f, 1.0f); glVertex2d(i + 1, j);
				glTexCoord2f(1.0f, 0.0f); glVertex2d(i + 1, j + 1);
				glTexCoord2f(0.0f, 0.0f); glVertex2d(i, j + 1);
				glEnd();
				glDisable(GL_TEXTURE_2D);
				glPopMatrix();
				glutPostRedisplay();
				break;

			case '~': // mur Level 3

				glPushMatrix();
				glEnable(GL_TEXTURE_2D);
				glEnable(GL_BLEND);
				glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
				glBindTexture(GL_TEXTURE_2D, texture[96]);
				glBegin(GL_QUADS);
				glColor4d(1.0, 1.0, 1.0, 1.0);
				glTexCoord2f(0.0f, 1.0f); glVertex2d(i, j);
				glTexCoord2f(1.0f, 1.0f); glVertex2d(i + 1, j);
				glTexCoord2f(1.0f, 0.0f); glVertex2d(i + 1, j + 1);
				glTexCoord2f(0.0f, 0.0f); glVertex2d(i, j + 1);
				glEnd();
				glDisable(GL_TEXTURE_2D);
				glPopMatrix();
				glutPostRedisplay();
				break;

			case '2': // mur destructible
				int tex1;
				if (niveau == 1) {
					tex1 = 22;
				}
				else if (niveau == 2) {
					tex1 = 23;
				}
				else if (niveau == 3) {
					tex1 = 24;
				}
				glPushMatrix();
				glEnable(GL_TEXTURE_2D);
				glEnable(GL_BLEND);
				glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
				glBindTexture(GL_TEXTURE_2D, texture[tex1]);
				glBegin(GL_QUADS);
				glColor4d(1.0, 1.0, 1.0, 1.0);
				glTexCoord2f(0.0f, 1.0f); glVertex2d(i, j);
				glTexCoord2f(1.0f, 1.0f); glVertex2d(i + 1, j);
				glTexCoord2f(1.0f, 0.0f); glVertex2d(i + 1, j + 1);
				glTexCoord2f(0.0f, 0.0f); glVertex2d(i, j + 1);
				glEnd();
				glDisable(GL_TEXTURE_2D);
				glPopMatrix();
				glutPostRedisplay();
				break;

			case '#': //Bombe pose
				int tex2;
				if (niveau == 1) {
					tex2 = 63;
				}
				else if (niveau == 2) {
					tex2 = 14;
				}
				else if (niveau == 3) {
					tex2 = 87;
				}
				glPushMatrix();
				glEnable(GL_TEXTURE_2D);
				glEnable(GL_BLEND);
				glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
				glBindTexture(GL_TEXTURE_2D, texture[tex2]);
				glBegin(GL_QUADS);
				glColor3d(1.0, 1.0, 1.0);
				glTexCoord2f(0.0f, 0.0f); glVertex2d(i, j);
				glTexCoord2f(1.0f, 0.0f); glVertex2d(i + 1, j);
				glTexCoord2f(1.0f, 1.0f); glVertex2d(i + 1, j + 1);
				glTexCoord2f(0.0f, 1.0f); glVertex2d(i, j + 1);
				glEnd();
				glDisable(GL_TEXTURE_2D);
				glPopMatrix();
				glutPostRedisplay();
				break;

			case '^': //Bombe explose centre
				int tex3;
				if (niveau == 1) {
					tex3 = 64;
				}
				else if (niveau == 2) {
					tex3 = 15;
				}
				else if (niveau == 3) {
					tex3 = 88;
				}
				glPushMatrix();
				glEnable(GL_TEXTURE_2D);
				glEnable(GL_BLEND);
				glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
				glBindTexture(GL_TEXTURE_2D, texture[tex3]);
				glBegin(GL_QUADS);
				glColor4d(1.0, 1.0, 1.0, 1.0);
				glTexCoord2f(0.0f, 0.0f); glVertex2d(i, j);
				glTexCoord2f(1.0f, 0.0f); glVertex2d(i + 1, j);
				glTexCoord2f(1.0f, 1.0f); glVertex2d(i + 1, j + 1);
				glTexCoord2f(0.0f, 1.0f); glVertex2d(i, j + 1);
				glEnd();
				glDisable(GL_TEXTURE_2D);
				glPopMatrix();
				glutPostRedisplay();
				break;

			case '3': // explosion autour de la bombe DROITE / GAUCHE
				int tex4;
				if (niveau == 1) {
					tex4 = 66;
				}
				else if (niveau == 2) {
					tex4 = 17;
				}
				else if (niveau == 3) {
					tex4 = 90;
				}
				glPushMatrix();
				glEnable(GL_TEXTURE_2D);
				glEnable(GL_BLEND);
				glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
				glBindTexture(GL_TEXTURE_2D, texture[tex4]);
				glBegin(GL_QUADS);
				glColor4d(1.0, 1.0, 1.0, 1.0);
				glTexCoord2f(0.0f, 0.0f); glVertex2d(i, j);
				glTexCoord2f(1.0f, 0.0f); glVertex2d(i + 1, j);
				glTexCoord2f(1.0f, 1.0f); glVertex2d(i + 1, j + 1);
				glTexCoord2f(0.0f, 1.0f); glVertex2d(i, j + 1);
				glEnd();
				glDisable(GL_TEXTURE_2D);
				glPopMatrix();
				glutPostRedisplay();
				break;

			case '4': // derni�re explosion de la bombe DROITE
				int tex5;
				if (niveau == 1) {
					tex5 = 70;
				}
				else if (niveau == 2) {
					tex5 = 21;
				}
				else if (niveau == 3) {
					tex5 = 94;
				}
				glPushMatrix();
				glEnable(GL_TEXTURE_2D);
				glEnable(GL_BLEND);
				glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
				glBindTexture(GL_TEXTURE_2D, texture[tex5]);
				glBegin(GL_QUADS);
				glColor4d(1.0, 1.0, 1.0, 1.0);
				glTexCoord2f(0.0f, 0.0f); glVertex2d(i, j);
				glTexCoord2f(1.0f, 0.0f); glVertex2d(i + 1, j);
				glTexCoord2f(1.0f, 1.0f); glVertex2d(i + 1, j + 1);
				glTexCoord2f(0.0f, 1.0f); glVertex2d(i, j + 1);
				glEnd();
				glDisable(GL_TEXTURE_2D);
				glPopMatrix();
				glutPostRedisplay();
				break;

			case '5': // derni�re explosion de la bombe GAUCHE
				int tex6;
				if (niveau == 1) {
					tex6 = 69;
				}
				else if (niveau == 2) {
					tex6 = 20;
				}
				else if (niveau == 3) {
					tex6 = 93;
				}
				glPushMatrix();
				glEnable(GL_TEXTURE_2D);
				glEnable(GL_BLEND);
				glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
				glBindTexture(GL_TEXTURE_2D, texture[tex6]);
				glBegin(GL_QUADS);
				glColor4d(1.0, 1.0, 1.0, 1.0);
				glTexCoord2f(0.0f, 0.0f); glVertex2d(i, j);
				glTexCoord2f(1.0f, 0.0f); glVertex2d(i + 1, j);
				glTexCoord2f(1.0f, 1.0f); glVertex2d(i + 1, j + 1);
				glTexCoord2f(0.0f, 1.0f); glVertex2d(i, j + 1);
				glEnd();
				glDisable(GL_TEXTURE_2D);
				glPopMatrix();
				glutPostRedisplay();
				break;
			
			case '6': // explosion autour de la bombe HAUT / BAS
				int tex7;
				if (niveau == 1) {
					tex7 = 65;
				}
				else if (niveau == 2) {
					tex7 = 16;
				}
				else if (niveau == 3) {
					tex7 = 89;
				}
				glPushMatrix();
				glEnable(GL_TEXTURE_2D);
				glEnable(GL_BLEND);
				glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
				glBindTexture(GL_TEXTURE_2D, texture[tex7]);
				glBegin(GL_QUADS);
				glColor4d(1.0, 1.0, 1.0, 1.0);
				glTexCoord2f(0.0f, 0.0f); glVertex2d(i, j);
				glTexCoord2f(1.0f, 0.0f); glVertex2d(i + 1, j);
				glTexCoord2f(1.0f, 1.0f); glVertex2d(i + 1, j + 1);
				glTexCoord2f(0.0f, 1.0f); glVertex2d(i, j + 1);
				glEnd();
				glDisable(GL_TEXTURE_2D);
				glPopMatrix();
				glutPostRedisplay();
				break;

			case '7': //derni�re explosion de la bombe HAUT
				int tex8;
				if (niveau == 1) {
					tex8 = 67;
				}
				else if (niveau == 2) {
					tex8 = 18;
				}
				else if (niveau == 3) {
					tex8 = 91;
				}
				glPushMatrix();
				glEnable(GL_TEXTURE_2D);
				glEnable(GL_BLEND);
				glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
				glBindTexture(GL_TEXTURE_2D, texture[tex8]);
				glBegin(GL_QUADS);
				glColor4d(1.0, 1.0, 1.0, 1.0);
				glTexCoord2f(0.0f, 0.0f); glVertex2d(i, j);
				glTexCoord2f(1.0f, 0.0f); glVertex2d(i + 1, j);
				glTexCoord2f(1.0f, 1.0f); glVertex2d(i + 1, j + 1);
				glTexCoord2f(0.0f, 1.0f); glVertex2d(i, j + 1);
				glEnd();
				glDisable(GL_TEXTURE_2D);
				glPopMatrix();
				glutPostRedisplay();
				break;

			case '8' : //derni�re explosion de la bombe BAS
				int tex9;
				if (niveau == 1) {
					tex9 = 68;
				}
				else if (niveau == 2) {
					tex9 = 19;
				}
				else if (niveau == 3) {
					tex9 = 92;
				}
				glPushMatrix();
				glEnable(GL_TEXTURE_2D);
				glEnable(GL_BLEND);
				glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
				glBindTexture(GL_TEXTURE_2D, texture[tex9]);
				glBegin(GL_QUADS);
				glColor4d(1.0, 1.0, 1.0, 1.0);
				glTexCoord2f(0.0f, 0.0f); glVertex2d(i, j);
				glTexCoord2f(1.0f, 0.0f); glVertex2d(i + 1, j);
				glTexCoord2f(1.0f, 1.0f); glVertex2d(i + 1, j + 1);
				glTexCoord2f(0.0f, 1.0f); glVertex2d(i, j + 1);
				glEnd();
				glDisable(GL_TEXTURE_2D);
				glPopMatrix();
				glutPostRedisplay();
				break;

			case '*': //BONNUS Vitesse
				int bonus1;
				if (niveau == 1) {
					bonus1 = 71;
				}
				else if (niveau == 2) {
					bonus1 = 30;
				}
				else if (niveau == 3) {
					bonus1 = 71;
				}
				glPushMatrix();
				glEnable(GL_TEXTURE_2D);
				glEnable(GL_BLEND);
				glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
				glBindTexture(GL_TEXTURE_2D, texture[bonus1]);
				glBegin(GL_QUADS);
				glColor4d(1.0, 1.0, 1.0, 1.0);
				glTexCoord2f(0.0f, 1.0f); glVertex2d(i, j);
				glTexCoord2f(1.0f, 1.0f); glVertex2d(i + 1, j);
				glTexCoord2f(1.0f, 0.0f); glVertex2d(i + 1, j + 1);
				glTexCoord2f(0.0f, 0.0f); glVertex2d(i, j + 1);
				glEnd();
				glDisable(GL_TEXTURE_2D);
				glPopMatrix();
				glutPostRedisplay();
				break;

			case '/': //BONNUS Distance bombe
				int bonus2;
				if (niveau == 1) {
					bonus2 = 74;
				}
				else if (niveau == 2) {
					bonus2 = 33;
				}
				else if (niveau == 3) {
					bonus2 = 74;
				}
				glPushMatrix();
				glEnable(GL_TEXTURE_2D);
				glEnable(GL_BLEND);
				glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
				glBindTexture(GL_TEXTURE_2D, texture[bonus2]);
				glBegin(GL_QUADS);
				glColor4d(1.0, 1.0, 1.0, 1.0);
				glTexCoord2f(0.0f, 1.0f); glVertex2d(i, j);
				glTexCoord2f(1.0f, 1.0f); glVertex2d(i + 1, j);
				glTexCoord2f(1.0f, 0.0f); glVertex2d(i + 1, j + 1);
				glTexCoord2f(0.0f, 0.0f); glVertex2d(i, j + 1);
				glEnd();
				glDisable(GL_TEXTURE_2D);
				glPopMatrix();
				glutPostRedisplay();
				break;

			case '+': //BONNUS Bombe
				int bonus3;
				if (niveau == 1) {
					bonus3 = 72;
				}
				else if (niveau == 2) {
					bonus3 = 31;
				}
				else if (niveau == 3) {
					bonus3 = 72;
				}
				glPushMatrix();
				glEnable(GL_TEXTURE_2D);
				glEnable(GL_BLEND);
				glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
				glBindTexture(GL_TEXTURE_2D, texture[bonus3]);
				glBegin(GL_QUADS);
				glColor4d(1.0, 1.0, 1.0, 1.0);
				glTexCoord2f(0.0f, 1.0f); glVertex2d(i, j);
				glTexCoord2f(1.0f, 1.0f); glVertex2d(i + 1, j);
				glTexCoord2f(1.0f, 0.0f); glVertex2d(i + 1, j + 1);
				glTexCoord2f(0.0f, 0.0f); glVertex2d(i, j + 1);
				glEnd();
				glDisable(GL_TEXTURE_2D);
				glPopMatrix();
				glutPostRedisplay();
				break;

			case '&': //BONNUS Vie supppl�mentaire
				int bonus4;
				if (niveau == 1) {
					bonus4 = 73;
				}
				else if (niveau == 2) {
					bonus4 = 32;
				}
				else if (niveau == 3) {
					bonus4 = 73;
				}
				glPushMatrix();
				glEnable(GL_TEXTURE_2D);
				glEnable(GL_BLEND);
				glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
				glBindTexture(GL_TEXTURE_2D, texture[bonus4]);
				glBegin(GL_QUADS);
				glColor4d(1.0, 1.0, 1.0, 1.0);
				glTexCoord2f(0.0f, 1.0f); glVertex2d(i, j);
				glTexCoord2f(1.0f, 1.0f); glVertex2d(i + 1, j);
				glTexCoord2f(1.0f, 0.0f); glVertex2d(i + 1, j + 1);
				glTexCoord2f(0.0f, 0.0f); glVertex2d(i, j + 1);
				glEnd();
				glDisable(GL_TEXTURE_2D);
				glPopMatrix();
				glutPostRedisplay();
				break;

			case '@': //KEY

				glPushMatrix();
				glEnable(GL_TEXTURE_2D);
				glEnable(GL_BLEND);
				glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
				glBindTexture(GL_TEXTURE_2D, texture[34]);
				glBegin(GL_QUADS);
				glColor4d(1.0, 1.0, 1.0, 1.0);
				glTexCoord2f(1.0f, 1.0f); glVertex2d(i, j);
				glTexCoord2f(0.0f, 1.0f); glVertex2d(i + 1, j);
				glTexCoord2f(0.0f, 0.0f); glVertex2d(i + 1, j + 1);
				glTexCoord2f(1.0f, 0.0f); glVertex2d(i, j + 1);
				glEnd();
				glDisable(GL_TEXTURE_2D);
				glPopMatrix();
				glutPostRedisplay();
				break;
			
			case '|': //DOOR
				int tex10_0;
				int tex10_1;
				if (niveau == 1) {
					tex10_0 = 29;
					tex10_1 = 43;
				}
				else if (niveau == 2) {
					tex10_0 = 29;
					tex10_1 = 43;
				}
				else if (niveau == 3) {
					tex10_0 = 29;
					tex10_1 = 43;
				}
				if (player.ReturnKey() == false) {
					glPushMatrix();
					glEnable(GL_TEXTURE_2D);
					glEnable(GL_BLEND);
					glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
					glBindTexture(GL_TEXTURE_2D, texture[tex10_0]);
					glBegin(GL_QUADS);
					glColor4d(1.0, 1.0, 1.0, 1.0);
					glTexCoord2f(0.0f, 0.0f); glVertex2d(i, j);
					glTexCoord2f(1.0f, 0.0f); glVertex2d(i + 1, j);
					glTexCoord2f(1.0f, 1.0f); glVertex2d(i + 1, j + 1);
					glTexCoord2f(0.0f, 1.0f); glVertex2d(i, j + 1);
					glEnd();
					glDisable(GL_TEXTURE_2D);
					glPopMatrix();
					glutPostRedisplay();
					break;
				}
				else {
					glPushMatrix();
					glEnable(GL_TEXTURE_2D);
					glEnable(GL_BLEND);
					glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
					glBindTexture(GL_TEXTURE_2D, texture[tex10_1]);
					glBegin(GL_QUADS);
					glColor4d(1.0, 1.0, 1.0, 1.0);
					glTexCoord2f(0.0f, 0.0f); glVertex2d(i, j);
					glTexCoord2f(1.0f, 0.0f); glVertex2d(i + 1, j);
					glTexCoord2f(1.0f, 1.0f); glVertex2d(i + 1, j + 1);
					glTexCoord2f(0.0f, 1.0f); glVertex2d(i, j + 1);
					glEnd();
					glDisable(GL_TEXTURE_2D);
					glPopMatrix();
					glutPostRedisplay();
					break;
				}


			}
		}
	}
}

void Niveau::Key() {
	int m=0;
	int nbMur = 0;
	for (int i = 0; i < NbLignes; i++) {
		for (int j = 0; j < NbColonnes; j++) {
			if (Matrice[j][i] == '2') {
				nbMur++;
			}
		}
	}

	srand(time(NULL));
	int r = rand() % nbMur + 1;

	for (int i = 0; i < NbLignes; i++) {
		for (int j = 0; j < NbColonnes; j++) {
			if (Matrice[j][i] == '2') {
				m++;
				if (m == r) {
					Matrice[j][i];
					keyX = i;
					keyY = j;
				}
				 
			}
		}
	}


		
	
}

