/////////////////////////////////////////////////////////////
//														   //
//					PROJET BOMBERMAN 					   //
//														   //
//				----- Class Bonus -----				       //
//														   //
/////////////////////////////////////////////////////////////

#include "Bonus.h"
#include "Bombe.h"
#include "avatar.h"

using namespace std;

extern int LARGEUR;
extern int HAUTEUR;
extern char** Matrice;

extern vector<GLuint> texture; 
extern avatar player;
extern vector<Bombe> bombe;
extern int tailleTableauBombe;

Bonus::Bonus()
{
	vitesse = 1;
	distance = 1;
	vie = 3;
	nbBombe = 1;
	
}

void Bonus::UpdateVitesse()
{
	vitesse+=1;
	player.vitesse += 0.05;
}

void Bonus::UpdateNbBombe()
{
	player.nbBombe++;
	Bombe object = Bombe::Bombe();
	bombe.push_back(object);
	tailleTableauBombe = bombe.size();
	for (int i = 0; i < bombe.size(); i++) {
		bombe[i].UpdateDistance(distance);
	}
	nbBombe++;
}

void Bonus::UpdateDistance()
{
	distance++;
	for (int i = 0; i < bombe.size(); i++) {
		bombe[i].distance = distance ;
		bombe[i].UpdateDistance(distance);
	}
}

void Bonus::UpdateVie()
{
	vie++;
	player.nbVies++;
}

int Bonus::ReturnDistance()
{
	return distance;
}

int Bonus::ReturnVitesse()
{
	return vitesse;
}

int Bonus::ReturnNbBombe()
{
	return nbBombe;
}

void Bonus::Reset() {
	for (int i = 0; i < bombe.size()-1; i++) {
		bombe.pop_back();
		
	}
	tailleTableauBombe = bombe.size();
	vitesse = 1;
	nbBombe = 1;
	distance = 1;
	vie = 3;
	for (int i = 0; i < bombe.size() - 1; i++) {
		bombe[i].distance = 1;

	}
}

