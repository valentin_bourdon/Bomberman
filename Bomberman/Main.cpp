/////////////////////////////////////////////////////////////
//														   //
//				PROJET BOMBERMAN  						   //
//														   //
//				----- Fichier principal -----			   //
//														   //
/////////////////////////////////////////////////////////////


#include <vector>
#include <fstream>
#include <iostream>
#include <windows.h> 
#include <sstream>
#include <string>
#include "Bonus.h"
#include "avatar.h"
#include "Bombe.h"
#include "Niveau.h"
#include "ennemi.h"
#include "Timer.h"
#include <time.h>
#include "SOIL/SOIL.h"
#include "GL/glut.h"
#include "FMOD/fmod.h"
#include <stdlib.h>
#include <stdio.h>
#include <tchar.h>
#include "SerialClass.h"	
#include <iomanip>
using std::setfill;
using std::setw;

/////////////////////////////////////////////// VARIABLES ///////////////////////////////////////////////

int LARGEUR= 1920; // Largeur de la fen�tre
int HAUTEUR = 1200; // Hauteur de la fen�tre

using namespace std;


/*SOUND*/
float  volume = 1.0;
FMOD_SOUND *musique;
FMOD_RESULT resultat;
FMOD_CHANNELGROUP *  channelgroup;


/*TIMER*/
int hour = 0, minutes = 0, sec = 0; 
int minute = 4;
int seconde = 0;
bool startclock = false;   
string timestring = "04:00";



/*ARDUINO*/
Serial* SP = new Serial("COM5");
char incomingData[256] = "";			
int dataLength = 256;
int readResult = 0;



/*Niveau*/
Niveau level;
ifstream fichier; // Objet de type ifstream, pour lire un fichier extern
char** Matrice; // Matrice contenant le niveau

/*Joueur*/
avatar player(1,1,2);

/*Bonnus*/
Bonus bonnus;

/*Timer*/
Timer aTimer;
clock_t t;

/*Bombe*/
int tailleTableauBombe;
vector<Bombe> bombe;
Bombe boum;


/*Ennemi*/
int stockTime;
bool depTime;
const int nbVaisseau = 4;
const int nbJawa = 4;
const int nbStormtrooper = 4;
int nbEnnemiLevel1 = nbJawa;
int nbEnnemiLevel2 = nbVaisseau;
int nbEnnemiLevel3 = nbStormtrooper;
ennemi vaisseaux[nbVaisseau];
ennemi jawa[nbJawa];
ennemi stormtrooper[nbStormtrooper];


int r[4];  // Nombre al�atoire pour le d�placement des ennemis
int vitesse = 300; // Vitesse de d�placement des ennemis
bool touch; //variable booleen pour savoir si le joueur est touch� par un ennemi ou par l'explosion d'une bombe

/*Game*/
bool start; //variable booleen pour le lancement du jeu
bool gameOver;
int imgFailed=98;
bool win;
bool pause;
int page; // variable pour faire passer les images (accueil, cin�matique)
vector<GLuint>	texture;
void TraitementNiveau();



/////////////////////////////////////////////// FONCTIONS ///////////////////////////////////////////////




//////////////////////////////////
/*------------SOUND-------------*/
/////////////////////////////////

void Musique(string _musique, const char *_chemin) {
	FMOD_SYSTEM *system;
	
	FMOD_System_Create(&system);
	FMOD_System_Init(system, 1, FMOD_INIT_NORMAL, NULL);

	_musique = FMOD_System_CreateSound(system, _chemin, FMOD_SOFTWARE | FMOD_2D | FMOD_CREATESTREAM, 0, &musique);
	FMOD_System_PlaySound(system, FMOD_CHANNEL_FREE, musique, 0, NULL);
	FMOD_System_GetMasterChannelGroup(system, &channelgroup);
	FMOD_ChannelGroup_SetVolume(channelgroup, volume);
	FMOD_Sound_SetLoopCount(musique, -1);
}

void PauseMusique(int _volume) {
	FMOD_ChannelGroup_SetPaused(channelgroup, _volume);
}

void StopMusique(FMOD_SOUND *musique) {
	FMOD_Sound_Release(musique);
}

void UpdateVolume(float _sound) {
	FMOD_ChannelGroup_SetVolume(channelgroup, _sound);
}



//Fonction Timer
void myTimer(int v)     // timer function to update time
{
	ostringstream timestream;


	if (startclock) {
		sec++;         // increment second
		if (sec == 60) { minutes++; sec = 00; } // increment minute
		if (minutes == 60) { hour = hour++ % 24; minutes = 00; } //increment hour



		if (seconde == 0) { minute--; seconde = 60; }
		seconde--;

		timestream << setfill('0') << setw(2)
			<< minute << ":" << setw(2) << seconde;
		timestring = timestream.str();  //convert stream to string
	}

	
	glutPostRedisplay();
	glutTimerFunc(1000 / v, myTimer, v);  //repost timer 
}


// Fonction pour afficher des caract�res dans la fen�tre // 
void vBitmapOutput(float x, float y, char *string, void *font)
{
	int len, i; // len donne la longueur de la cha�ne de caract�res

	glRasterPos2f(x, y); // Positionne le premier caract�re de la cha�ne
	len = (int)strlen(string); // Calcule la longueur de la cha�ne
	for (i = 0; i < len; i++) glutBitmapCharacter(font, string[i]); // Affiche chaque caract�re de la cha�ne
}
void printbitmap(const string msg, double x, double y)
{
	glRasterPos2d(x, y);
	for (string::const_iterator ii = msg.begin(); ii != msg.end(); ++ii)
	{
		glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, *ii);
	}
}


// Fonction pour t�l�charger des bitmap et les convertir en texture
int LoadGLTextures(string name)
{
	GLuint essai = SOIL_load_OGL_texture
		(
			name.c_str(),
			SOIL_LOAD_AUTO,
			SOIL_CREATE_NEW_ID,
			SOIL_FLAG_INVERT_Y
			);

	texture.push_back(essai);

	if (texture.at(texture.size()-1) == 0)
		return false;

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	return true;    // Return Success
}



/*DEP ENNEMI*/
void DeplacementEnnemi() {
	
	if (level.niveau == 1) {
		for (int i = 0; i < nbJawa; i++) {
			if ((pause == false && start == true) && jawa[i].ReturnVie() >= 1 && jawa[i].touch == false) {


				if (jawa[i].depD == true) {
					jawa[i].DepDroite(i);
				}
				else if (jawa[i].depG == true) {
					jawa[i].DepGauche(i);
				}
				else if (jawa[i].depH == true) {
					jawa[i].DepTop(i);
				}
				else if (jawa[i].depB == true) {
					jawa[i].DepBottom(i);
				}
				else {
					jawa[i].DeplacementEnnemi(jawa[i].PositionX(), jawa[i].PositionY(), i, 300);  //passage en param�tre de la position de l'�nnemi et de la variable al�atoire r pour le d�placement de l'ennemi
				}

			}
		}
	}
	if (level.niveau == 2) {
		for (int i = 0; i < nbVaisseau; i++) {
			if ((pause == false && start == true) && vaisseaux[i].ReturnVie() >= 1 && vaisseaux[i].touch == false) {


				if (vaisseaux[i].depD == true) {
					vaisseaux[i].DepDroite(i);
				}
				else if (vaisseaux[i].depG == true) {
					vaisseaux[i].DepGauche(i);
				}
				else if (vaisseaux[i].depH == true) {
					vaisseaux[i].DepTop(i);
				}
				else if (vaisseaux[i].depB == true) {
					vaisseaux[i].DepBottom(i);
				}
				else {
					vaisseaux[i].DeplacementEnnemi(vaisseaux[i].PositionX(), vaisseaux[i].PositionY(), i, 300);  //passage en param�tre de la position de l'�nnemi et de la variable al�atoire r pour le d�placement de l'ennemi
				}

			}
		}
	}
	if (level.niveau == 3) {
		for (int i = 0; i < nbStormtrooper; i++) {
			if ((pause == false && start == true) && stormtrooper[i].ReturnVie() >= 1 && stormtrooper[i].touch == false) {


				if (stormtrooper[i].depD == true) {
					stormtrooper[i].DepDroite(i);
				}
				else if (stormtrooper[i].depG == true) {
					stormtrooper[i].DepGauche(i);
				}
				else if (stormtrooper[i].depH == true) {
					stormtrooper[i].DepTop(i);
				}
				else if (stormtrooper[i].depB == true) {
					stormtrooper[i].DepBottom(i);
				}
				else {
					stormtrooper[i].DeplacementEnnemi(stormtrooper[i].PositionX(), stormtrooper[i].PositionY(), i, 300);  //passage en param�tre de la position de l'�nnemi et de la variable al�atoire r pour le d�placement de l'ennemi
				}

			}
		}
	}
	
	
}

/*TIMER BOMBE*/
void timerBombeExplose(int value) {
	PlaySound(NULL, 0, 0);
	for (int i = 0; i <bombe.size(); i++) {
		if ((bombe[i].explose == true) && (bombe[i].timer == true)) {
			bombe[i].explose = false;
			bombe[i].timer = false;
			bombe[i].Destroy();
			bombe[i].startTime = aTimer.Temps();
			
			tailleTableauBombe = i+1;
		}
	}

}
void timerPoseBombe(int nbBombe) {

	bombe[nbBombe].TimerExplose();
	StopMusique(musique);
	PlaySound(TEXT("sound/explosion.wav"), NULL, SND_ASYNC | NULL);
	
}

/*TRAITEMENT VIE*/
void TraitementViePlayer(int value) {
	if (touch == true) {
		player.UpdateVie();
		player.touch = false;
		touch = false;
	}
}
void TraitementVieEnnemi(int value) {
	
	if (level.niveau == 1) {
		for (int i = 0; i < nbJawa; i++) {
			if (jawa[i].touch == true) {
				jawa[i].UpdateVie();
				jawa[i].touch = false;
				if (jawa[i].ReturnVie() == 0)nbEnnemiLevel2--;
			}
		}
	}
	if (level.niveau == 2) {
		for (int i = 0; i < nbVaisseau; i++) {
			if (vaisseaux[i].touch == true) {
				vaisseaux[i].UpdateVie();
				vaisseaux[i].touch = false;
				if (vaisseaux[i].ReturnVie() == 0)nbEnnemiLevel2--;
			}
		}
	}
	if (level.niveau == 3) {
		for (int i = 0; i < nbStormtrooper; i++) {
			if (stormtrooper[i].touch == true) {
				stormtrooper[i].UpdateVie();
				stormtrooper[i].touch = false;
				if (stormtrooper[i].ReturnVie() == 0)nbEnnemiLevel3--;
			}
		}
	}
	

}


void restart(int _level) {

	minute = 4;
	seconde = 0;
	hour = minutes = sec = 0;
	pause = false;
	win = false;
	gameOver = false;
	PlaySound(NULL, 0, 0);
	UpdateVolume(1);
	PauseMusique(0);


	level.niveau = _level;
	if (_level == 1) {
		
		
		startclock = true; //Time
		start = true;
		page == 2;

		player.Reset(1, 1, 2);
		jawa[0].ResetPos(9, 1, 1);
		jawa[1].ResetPos(5, 5, 1);
		jawa[2].ResetPos(7, 7, 1);
		jawa[3].ResetPos(1, 9, 1);

		bonnus.Reset();

		level.niveau = 1;
		level.OuvrirNiveau("niveaux/niveau1.txt");
		level.Key();
	}
	else if (_level == 2) {
		
		startclock = true;
		start = true;
		page == 4;

		player.Reset(5, 1, 6);
		vaisseaux[0].ResetPos(9, 1, 2);
		vaisseaux[1].ResetPos(2, 3, 2);
		vaisseaux[2].ResetPos(6, 5, 2);
		vaisseaux[3].ResetPos(6, 9, 2);

		bonnus.Reset();

		level.niveau = 2;
		level.OuvrirNiveau("niveaux/niveau2.txt");
		level.Key();
	}
	else if (_level == 3){
		
		startclock = true;
		start = true;
		page == 6;

		player.Reset(1, 9, 10);
		stormtrooper[0].ResetPos(3, 2, 2);
		stormtrooper[1].ResetPos(8, 3, 2);
		stormtrooper[2].ResetPos(5, 5, 2);
		stormtrooper[3].ResetPos(9, 9, 2);

		bonnus.Reset();

		level.niveau = 3;
		level.Key();
		level.OuvrirNiveau("niveaux/niveau3.txt");
	}

}


//////////////////////////////////
/*----------AFFICHAGE----------*/
/////////////////////////////////

// Fonction qui affiche une texture
void AfficheTexture(int img) {
	
	glViewport(0, 0, LARGEUR, HAUTEUR);
	glLoadIdentity();

	glPushMatrix();

	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, texture[img]);
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 0.0f); glVertex2d(11, 11);
	glTexCoord2f(0.0f, 0.0f); glVertex2d(0, 11);
	glTexCoord2f(0.0f, 1.0f); glVertex2d(0, 0);
	glTexCoord2f(1.0f, 1.0f); glVertex2d(11, 0);
	glEnd();
	glDisable(GL_TEXTURE_2D);

	glPopMatrix();
	glutPostRedisplay();
}

// Fonction qui affiche le background du jeu
void afficheBackground() {
	//////AFFICHAGE DU BACKGROUND ///
	int back;
	if (level.niveau == 1) {
		back = 60;
	}
	else if (level.niveau==2) {
		back = 61;
	}
	else if (level.niveau == 3) {
		back = 62;
	}
	
	glViewport(0, 0, LARGEUR, HAUTEUR );
	glLoadIdentity();

	glPushMatrix();

	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glBindTexture(GL_TEXTURE_2D, texture[back]);
	glBegin(GL_QUADS);
	glColor4d(1.0, 1.0, 1.0, 1.0);
	glTexCoord2f(1.0f, 0.0f); glVertex2d(11, 11);
	glTexCoord2f(0.0f, 0.0f); glVertex2d(0, 11);
	glTexCoord2f(0.0f, 1.0f); glVertex2d(0, 0);
	glTexCoord2f(1.0f, 1.0f); glVertex2d(11, 0);
	glEnd();
	glDisable(GL_TEXTURE_2D);

	glPopMatrix();
	glutPostRedisplay();
}

// Fonction qui affiche le menu pause
void affichePause() {
	
	glViewport(LARGEUR / 4, HAUTEUR / 10, LARGEUR*0.5, HAUTEUR*0.8);
	glLoadIdentity();

	glPushMatrix();

	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, texture[86]);
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 0.0f); glVertex2d(11, 11);
	glTexCoord2f(0.0f, 0.0f); glVertex2d(0, 11);
	glTexCoord2f(0.0f, 1.0f); glVertex2d(0, 0);
	glTexCoord2f(1.0f, 1.0f); glVertex2d(11, 0);
	glEnd();
	glDisable(GL_TEXTURE_2D);

	glPopMatrix();
	glutPostRedisplay();
}



//////////////////////////////////
/*------TRAITEMENT TOUCHES------*/
/////////////////////////////////

void TraitementArduino(int value) {

	if(SP->IsConnected()) // cette ligne est une securit�
	{
		readResult = SP->ReadData(incomingData, dataLength);

		if (readResult != -1) { // cette ligne est une securit�
			
			//BOUTON POSE BOMBE
			if (incomingData[0] == '0') {
				
				if ((tailleTableauBombe>0) && start == true && pause==false && aTimer.IsPaused() == false && Matrice[player.PositionY()][player.PositionX()] != '|') {
					
					
					tailleTableauBombe = tailleTableauBombe - 1;
					bombe[tailleTableauBombe].pose = true;
					bombe[tailleTableauBombe].UpdatePosition(player.PositionX(), player.PositionY());
					bombe[tailleTableauBombe].startTime = aTimer.Temps();
					bombe[tailleTableauBombe].TimerBombe();
					Matrice[bombe[tailleTableauBombe].y][bombe[tailleTableauBombe].x] = '#';
					Musique("bombe", "sound/Bruitage.wav");

				}
				Sleep(150);
			}

			//BOUTON START
			if (incomingData[0] == '1') {
				if ((start == false) && gameOver == false && page != 7) {
					page++;
					if (page == 2) { //Start level 1
						startclock = true;
						win = false;
						start = true;
						jawa[0].ResetPos(9, 1, 1);
						jawa[1].ResetPos(5, 5, 1);
						jawa[2].ResetPos(7, 7, 1);
						jawa[3].ResetPos(1, 9, 1);


						StopMusique(musique);
						Musique("musiqueLevel1", "sound/StarWarsLevel1.mp3");

					}
					else if (page == 4) { //Start level 2
						minute = 4;
						seconde = 0;
						hour = minutes = sec = 0;
						pause = false;

						startclock = true;
						start = true;
						win = false;

						level.OuvrirNiveau("niveaux/niveau2.txt");
						level.niveau = 2;
						level.Key();
						player.Reset(5, 1, 6);
						vaisseaux[0].ResetPos(9, 1, 2);
						vaisseaux[1].ResetPos(2, 3, 2);
						vaisseaux[2].ResetPos(6, 5, 2);
						vaisseaux[3].ResetPos(6, 9, 2);

						bonnus.Reset();
						StopMusique(musique);
						Musique("musiqueLevel2", "sound/StarWarsLevel2.mp3");
					}
					else if (page == 6) { //Start level 3
						minute = 4;
						seconde = 0;
						hour = minutes = sec = 0;
						pause = false;
						startclock = true;
						start = true;
						win = false;

						level.OuvrirNiveau("niveaux/niveau3.txt");
						level.niveau = 3;
						level.Key();
						player.Reset(1, 9, 10);
						stormtrooper[0].ResetPos(3, 2, 2);
						stormtrooper[1].ResetPos(8, 3, 2);
						stormtrooper[2].ResetPos(5, 5, 2);
						stormtrooper[3].ResetPos(9, 9, 2);

						bonnus.Reset();
						StopMusique(musique);
						Musique("musiqueLevel3", "sound/StarWarsLevel3.mp3");
					}
				}

				//Mode Pause
				if (startclock == true && pause == false) {
					startclock = false;
					pause = true;
					PauseMusique(1);

				}
				else {
					startclock = true;
					pause = false;
					PauseMusique(0);

				}
				//Mode d�faite (restart ou quit)
				if ((gameOver == true) && imgFailed == 99) {  
					exit(0);
				}
				if ((gameOver == true) && imgFailed == 98) {  
					restart(level.niveau);
				}
				//Mode Victoire (quit)
				if ((win == true) && page == 10) {  
					exit(0);
				}
				Sleep(150);
			} 
			
			
			//HAUT
			if (incomingData[0] == '2' && pause==false) {
				
				player.DepTop();
				
			}
			//BAS
			if (incomingData[0] == '3'&& pause == false) {
				
				player.DepBottom();
				
			}

			//DROITE
			if (incomingData[0] == '4'&& pause == false) {
				
				if (gameOver == true) {
					imgFailed = 99;
					AfficheTexture(imgFailed);
				}
				else player.DepDroite();
				
			}

			//GAUCHE
			if (incomingData[0] == '5'&& pause == false) {
				
				if (gameOver == true) {
					imgFailed = 98;
					AfficheTexture(imgFailed);
				}
				else player.DepGauche();
				
			}

		}
	}
	glFlush();
	glutTimerFunc(50, TraitementArduino, 0);
}

void traitementClavier2(unsigned char key, int x, int y)
{

	if ((key == 32) && (tailleTableauBombe>0) && start == true && pause == false && Matrice[player.PositionY()][player.PositionX()] != '|') {  //touche espace
		

		
		tailleTableauBombe = tailleTableauBombe - 1;
		bombe[tailleTableauBombe].pose = true;
		
		Musique("bombe", "sound/Bruitage.wav");
		bombe[tailleTableauBombe].UpdatePosition(player.PositionX(), player.PositionY());
		bombe[tailleTableauBombe].startTime = aTimer.Temps();
		bombe[tailleTableauBombe].TimerBombe();
		Matrice[bombe[tailleTableauBombe].y][bombe[tailleTableauBombe].x] = '#';

	
	}
	
	if ((key == 27) && (start == true)) {  //touche echap
		if (startclock == true && pause == false) {
			startclock = false;
			pause = true;
			aTimer.Pause();
			PauseMusique(1);
			
		}
		else {
			startclock = true;
			pause = false;
			aTimer.Resume();
			PauseMusique(0);
			
		}
	}
	if ((key == 27) && (win == true) && page==7) {  //touche echap
		exit(0);
	}

	if ((key == 13) && (start == false) && gameOver==false && page!=7) {  //touche entr�e
		page++;
		if (page == 2) { //Start level 1
			startclock = true;
			win = false;
			start = true;
			jawa[0].ResetPos(9, 1, 1);
			jawa[1].ResetPos(5, 5, 1);
			jawa[2].ResetPos(7, 7, 1);
			jawa[3].ResetPos(1, 9, 1);

		
			StopMusique(musique);
			Musique("musiqueLevel1", "sound/StarWarsLevel1.mp3");

		}
		else if (page == 4) { //Start level 2
			minute = 4;
			seconde = 0;
			hour = minutes = sec = 0;
			pause = false;
			
			startclock = true;
			start = true;
			win = false;

			level.OuvrirNiveau("niveaux/niveau2.txt");
			level.niveau = 2;
			level.Key();
			player.Reset(5, 1, 6);
			vaisseaux[0].ResetPos(9, 1, 2);
			vaisseaux[1].ResetPos(2, 3, 2);
			vaisseaux[2].ResetPos(6, 5, 2);
			vaisseaux[3].ResetPos(6, 9, 2);

			bonnus.Reset();
			StopMusique(musique);
			Musique("musiqueLevel2", "sound/StarWarsLevel2.mp3");
		}
		else if (page == 6) { //Start level 3
			minute = 4;
			seconde = 0;
			hour = minutes = sec = 0;
			pause = false;
			startclock = true;
			start = true;
			win = false;

			level.OuvrirNiveau("niveaux/niveau3.txt");
			level.niveau = 3;
			level.Key();
			player.Reset(1, 9, 10);
			stormtrooper[0].ResetPos(3, 2, 2);
			stormtrooper[1].ResetPos(8, 3, 2);
			stormtrooper[2].ResetPos(5, 5, 2);
			stormtrooper[3].ResetPos(9, 9, 2);

			bonnus.Reset();
			StopMusique(musique);
			Musique("musiqueLevel3", "sound/StarWarsLevel3.mp3");
		}
	}

	if ((key == 13) && (gameOver == true) && imgFailed==99)exit(0); //Quitter le jeu
	if ((key == 13) && (gameOver == true) && imgFailed == 98)restart(level.niveau); //Restart Level

}

void traitementClavier(int key, int x, int y)
{

	if (key == GLUT_KEY_LEFT && start == true && startclock == true)player.DepGauche();
	if (key == GLUT_KEY_RIGHT && start == true && startclock == true)player.DepDroite();
	if (key == GLUT_KEY_UP && start == true && startclock == true) player.DepTop();
	if (key == GLUT_KEY_DOWN && start == true && startclock == true) player.DepBottom();

	if (key == GLUT_KEY_RIGHT && gameOver == true)imgFailed = 99;
	if (key == GLUT_KEY_LEFT && gameOver == true)imgFailed = 98;

	if (key == GLUT_KEY_F1 && volume>0) { 
		volume -= 0.1; 
		UpdateVolume(volume);
	}
	if (key == GLUT_KEY_F2 && volume < 1) { 
		volume += 0.1;
		UpdateVolume(volume);
		
	}

}


//////////////////////////////////
/*--------TRAITEMENT JEU--------*/
/////////////////////////////////

void TraitementNiveau(){
	
	
	 if ((start == false) && page==0) { //Accueil
		AfficheTexture(26);
	}
	 else if ((start == false) && page == 1) { //cin�matique 1
		 AfficheTexture(27);
	 }
	 else if ((start == false) && page == 3) { //cin�matique 2
		 AfficheTexture(28);
	 }
	 else if ((start == false) && page == 5) { //cin�matique 3
		 AfficheTexture(44);
	 }
	 else if ((start == false) && page == 7) { //WIN
		 AfficheTexture(45);
	 }
	
	
	 else if (start == true) { //JEU
		
		 aTimer.Start();
		 player.Dessiner();

		 
		

		 //////////////////////////////////
		 /*-------EVENEMENT BOMBE-------*/
		 /////////////////////////////////

		 for (int i = 0; i <  bombe.size(); i++) {
			 if ((bombe[i].pose == true) && pause ==false && (aTimer.Temps() + 3 < bombe[i].TimerBombe())) {
				 timerPoseBombe(i);
			 }
		 }

		 //-- Si l'explosion d'une bombe touche une autre bombe --//
		
		for (int i = 0; i < bombe.size(); i++) {
			if (Matrice[bombe[i].PositionY()][bombe[i].PositionX()] == '^' && bombe[i].explose == false) {
				bombe[i].TimerExplose();
				
			}
			
		 }
		

		//////////////////////////////////
		/*------EVENEMENTS ENNEMIS-----*/
		/////////////////////////////////

		 if (depTime == false) {
			 stockTime = aTimer.GetTicks();
			 depTime = true;
		 }
		 if (aTimer.GetTicks() + 80  < stockTime) {
			 DeplacementEnnemi();
			 depTime = false;
		 }
		 
		 if (level.niveau == 1) {
			 for (int i = 0; i < nbJawa; i++) {
				 if (jawa[i].ReturnVie() >= 1) {  //Affiche l'ennemi si celui est vivant
					 jawa[i].Dessiner();
				 }
				 else {
					 jawa[i].ResetPos(0, 0, 0);

				 }
			 }
		 }
		 if (level.niveau == 2) {
			 for (int i = 0; i < nbVaisseau; i++) {
				 if (vaisseaux[i].ReturnVie() >= 1) {  //Affiche l'ennemi si celui est vivant
					 vaisseaux[i].Dessiner();
				 }
				 else {
					 vaisseaux[i].ResetPos(0, 0, 0);

				 }
			 }
		 }
		 if (level.niveau == 3) {
			 for (int i = 0; i < nbStormtrooper; i++) {
				 if (stormtrooper[i].ReturnVie() >= 1) {  //Affiche l'ennemi si celui est vivant
					 stormtrooper[i].Dessiner();
				 }
				 else {
					 stormtrooper[i].ResetPos(0, 0, 0);

				 }
			 }
		 }
		

		 //////////////////////////////////
		 /*----------MENU PAUSE----------*/
		 /////////////////////////////////
		 if (startclock == false)affichePause(); //affichage du menu pause

		//////////////////////////////////
		/*---------VIE JOUEUR---------*/
		/////////////////////////////////
		
		 if (level.niveau == 1) {
			 for (int i = 0; i < nbJawa; i++) {
				 if ((player.PositionX() == jawa[i].PositionX()) && (player.PositionY() == jawa[i].PositionY()) ||
					 (Matrice[player.PositionY()][player.PositionX()] == '^') ||
					 (Matrice[player.PositionY()][player.PositionX()] == '3') ||
					 (Matrice[player.PositionY()][player.PositionX()] == '4') ||
					 (Matrice[player.PositionY()][player.PositionX()] == '5') ||
					 (Matrice[player.PositionY()][player.PositionX()] == '6') ||
					 (Matrice[player.PositionY()][player.PositionX()] == '7') ||
					 (Matrice[player.PositionY()][player.PositionX()] == '8')) {

					 player.touch = true;
					 touch = true;
					 glutTimerFunc(1000, TraitementViePlayer, 0);
				 }
			 }
		 }
		 if (level.niveau == 2) {
			 for (int i = 0; i < nbVaisseau; i++) {
				 if ((player.PositionX() == vaisseaux[i].PositionX()) && (player.PositionY() == vaisseaux[i].PositionY()) ||
					 (Matrice[player.PositionY()][player.PositionX()] == '^') ||
					 (Matrice[player.PositionY()][player.PositionX()] == '3') ||
					 (Matrice[player.PositionY()][player.PositionX()] == '4') ||
					 (Matrice[player.PositionY()][player.PositionX()] == '5') ||
					 (Matrice[player.PositionY()][player.PositionX()] == '6') ||
					 (Matrice[player.PositionY()][player.PositionX()] == '7') ||
					 (Matrice[player.PositionY()][player.PositionX()] == '8')) {

					 player.touch = true;
					 touch = true;
					 glutTimerFunc(1000, TraitementViePlayer, 0);
				 }
			 }
		 }
		 if (level.niveau == 3) {
			 for (int i = 0; i < nbStormtrooper; i++) {
				 if ((player.PositionX() == stormtrooper[i].PositionX()) && (player.PositionY() == stormtrooper[i].PositionY()) ||
					 (Matrice[player.PositionY()][player.PositionX()] == '^') ||
					 (Matrice[player.PositionY()][player.PositionX()] == '3') ||
					 (Matrice[player.PositionY()][player.PositionX()] == '4') ||
					 (Matrice[player.PositionY()][player.PositionX()] == '5') ||
					 (Matrice[player.PositionY()][player.PositionX()] == '6') ||
					 (Matrice[player.PositionY()][player.PositionX()] == '7') ||
					 (Matrice[player.PositionY()][player.PositionX()] == '8')) {

					 player.touch = true;
					 touch = true;
					 glutTimerFunc(1000, TraitementViePlayer, 0);
				 }
			 }
		 }
		 

		 //////////////////////////////////
		 /*---------VIE ENNEMIS---------*/
		 /////////////////////////////////

		 if (level.niveau == 1) {
			 for (int i = 0; i < nbJawa; i++) {
				 if ((Matrice[jawa[i].PositionY()][jawa[i].PositionX()] == '^') ||
					 (Matrice[jawa[i].PositionY()][jawa[i].PositionX()] == '3') ||
					 (Matrice[jawa[i].PositionY()][jawa[i].PositionX()] == '4') ||
					 (Matrice[jawa[i].PositionY()][jawa[i].PositionX()] == '5') ||
					 (Matrice[jawa[i].PositionY()][jawa[i].PositionX()] == '6') ||
					 (Matrice[jawa[i].PositionY()][jawa[i].PositionX()] == '7') ||
					 (Matrice[jawa[i].PositionY()][jawa[i].PositionX()] == '8')) {


					 jawa[i].touch = true;
					 glutTimerFunc(1000, TraitementVieEnnemi, 0);
				 }
			 }
		 }
		 if (level.niveau == 2) {
			 for (int i = 0; i < nbVaisseau; i++) {
				 if ((Matrice[vaisseaux[i].PositionY()][vaisseaux[i].PositionX()] == '^') ||
					 (Matrice[vaisseaux[i].PositionY()][vaisseaux[i].PositionX()] == '3') ||
					 (Matrice[vaisseaux[i].PositionY()][vaisseaux[i].PositionX()] == '4') ||
					 (Matrice[vaisseaux[i].PositionY()][vaisseaux[i].PositionX()] == '5') ||
					 (Matrice[vaisseaux[i].PositionY()][vaisseaux[i].PositionX()] == '6') ||
					 (Matrice[vaisseaux[i].PositionY()][vaisseaux[i].PositionX()] == '7') ||
					 (Matrice[vaisseaux[i].PositionY()][vaisseaux[i].PositionX()] == '8')) {


					 vaisseaux[i].touch = true;
					 glutTimerFunc(1000, TraitementVieEnnemi, 0);
				 }
			 }
		 }
		 if (level.niveau == 3) {
			 for (int i = 0; i < nbStormtrooper; i++) {
				 if ((Matrice[stormtrooper[i].PositionY()][stormtrooper[i].PositionX()] == '^') ||
					 (Matrice[stormtrooper[i].PositionY()][stormtrooper[i].PositionX()] == '3') ||
					 (Matrice[stormtrooper[i].PositionY()][stormtrooper[i].PositionX()] == '4') ||
					 (Matrice[stormtrooper[i].PositionY()][stormtrooper[i].PositionX()] == '5') ||
					 (Matrice[stormtrooper[i].PositionY()][stormtrooper[i].PositionX()] == '6') ||
					 (Matrice[stormtrooper[i].PositionY()][stormtrooper[i].PositionX()] == '7') ||
					 (Matrice[stormtrooper[i].PositionY()][stormtrooper[i].PositionX()] == '8')) {


					 stormtrooper[i].touch = true;
					 glutTimerFunc(1000, TraitementVieEnnemi, 0);
				 }
			 }
		 }

		 //////////////////////////////////
		 /*-----------DEFAITE-----------*/
		 /////////////////////////////////

		 if ((minute == 0 && seconde == 0) || (player.ReturnVie() == 0)) { //sup�rieur � 5 minute ou plus de vie, perdu, affichage de l'�cran de gameOver
			 aTimer.Stop(); //Arr�te le timer
			 
			 startclock = false;
			 start = false;
			 gameOver = true;
			 StopMusique(musique);
			 Musique("musiqueDefaite", "sound/StarWarsDefaite.wav");
		 }
		
		 //////////////////////////////////
		 /*-----------VICTOIRE-----------*/
		 /////////////////////////////////

		 /*Condition de victoire Niveau 1*/
		 if (level.niveau ==1 && Matrice[player.PositionY()][player.PositionX()] == '|' && player.ReturnKey()) {
			
			 SP->WriteData("W", 1);
			 start = false;
			 win = true;
			 page++;
			 StopMusique(musique);
			 Musique("musiqueDefaite", "sound/StarWarsWin.wav");
			
			
		 }
		

		 /*Condition de victoire Niveau 2*/
		 if (level.niveau == 2 && Matrice[player.PositionY()][player.PositionX()] == '|' && player.ReturnKey() && nbEnnemiLevel2==0) {
			
			 SP->WriteData("W", 1);
			 start = false;
			 win = true;
			 page++;
			 StopMusique(musique);
			 Musique("musiqueDefaite", "sound/StarWarsWin.wav");
			 
		 }
		

		 /*Condition de victoire Niveau 3*/
		 if (level.niveau == 3 && Matrice[player.PositionY()][player.PositionX()] == '|' && player.ReturnKey() && nbEnnemiLevel3 == 0) {
			 
			 SP->WriteData("W", 1);
			 win = true;
			 start = false;
			 page++;
			 StopMusique(musique);
			 Musique("musiqueWin", "sound/StarWarsWin.wav");
			 
		 }
		

		//////////////////////////////////
		/*-------------HUD-------------*/
		/////////////////////////////////
		
		glViewport(LARGEUR / 12, 0, LARGEUR / 3, HAUTEUR);
		glLoadIdentity();

		/*TEMPS*/
		printbitmap(timestring, 1.65, 4.4); // time string

		/*BONUS VIE*/
		glColor3d(1, 1, 1);
		string Vie = static_cast<ostringstream*>(&(ostringstream() << player.ReturnVie()))->str();  //Conversion de score (int) en une chaine de caract�re//
		char* v;
		v = (char*)Vie.c_str();	//Conversion d'une chaine de caract�re (string) en caract�re (char)
		vBitmapOutput(2, 5.3, v, GLUT_BITMAP_HELVETICA_18);  //Affichage d'un texte
		SP->WriteData(v, 1);


		/*BONUS NbBOMBE*/
		glColor3d(1, 1, 1);
		string Bombe = static_cast<ostringstream*>(&(ostringstream() << bonnus.ReturnNbBombe()))->str();  
		char* b;
		b = (char*)Bombe.c_str();	
		vBitmapOutput(2, 6.2, b, GLUT_BITMAP_HELVETICA_18);  

		/*BONUS DISTANCE*/
		glColor3d(1, 1, 1);
		string Distance = static_cast<ostringstream*>(&(ostringstream() << bonnus.ReturnDistance()))->str();  
		char* d;
		d = (char*)Distance.c_str();	
		vBitmapOutput(2, 7.1, d, GLUT_BITMAP_HELVETICA_18);  

		/*BONUS VITESSE*/
		glColor3d(1, 1, 1);
		string Speed = static_cast<ostringstream*>(&(ostringstream() << bonnus.ReturnVitesse()))->str();  
		char* s;
		s = (char*)Speed.c_str();	
		vBitmapOutput(2, 8, s, GLUT_BITMAP_HELVETICA_18);  

		/*KEY*/													 
		glColor3d(1, 1, 1);
		string Key = static_cast<ostringstream*>(&(ostringstream() << player.ReturnKey()))->str();  
		char* k;
		k = (char*)Key.c_str();	
		vBitmapOutput(2, 9, k, GLUT_BITMAP_HELVETICA_18); 
		
		/*NbENNEMI*/
		if (level.niveau == 2 ) {													 
			glColor3d(1, 1, 1);
			string NbEnnemi = static_cast<ostringstream*>(&(ostringstream() << nbEnnemiLevel2))->str();  
			char* e;
			e = (char*)NbEnnemi.c_str();	
			vBitmapOutput(2, 9.9, e, GLUT_BITMAP_HELVETICA_18);  
		}

		if (level.niveau == 3) {												 
			glColor3d(1, 1, 1);
			string NbEnnemi = static_cast<ostringstream*>(&(ostringstream() << nbEnnemiLevel3))->str();  
			char* e;
			e = (char*)NbEnnemi.c_str();	
			vBitmapOutput(2, 9.9, e, GLUT_BITMAP_HELVETICA_18);  
		}
		
	 }
	 else if (gameOver == true) {
		 AfficheTexture(imgFailed);
		 
	 }

}

void LabyAffichage()
{
	glClearColor(0, 0, 0, 0);
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	////////////////////////	Affiche le niveau	////////////////////////
	afficheBackground();
	level.DessinerNiveau();
	TraitementNiveau();

	glFlush();

}

void LabyRedim(int x, int y)
{
	glViewport(0, 0, x, y);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0.0, double(level.NbLignes), double(level.NbColonnes), 0.0);
	LARGEUR = x;
	HAUTEUR = y;
}




int _tmain(int argc, _TCHAR* argv[])
{
	cout << "Projet Info Bomberman realise par Bourdon Valentin, Moriet Thomas et Le Disez Corentin. \n";
	cout << "Master MTI3D \n";
	cout << "2016-2017 \n";

	//////////////////////////////////
	/*------------SOUND------------*/
	/////////////////////////////////

	FMOD_SYSTEM *system;
	FMOD_System_Create(&system);
	FMOD_System_Init(system, 1, FMOD_INIT_NORMAL, NULL);


	
	/* On ouvre la musique */
	resultat = FMOD_System_CreateSound(system, "sound/StarWars.mp3", FMOD_SOFTWARE | FMOD_2D | FMOD_CREATESTREAM, 0, &musique);
	

	/* On v�rifie si elle a bien �t� ouverte (IMPORTANT) */
	if (resultat != FMOD_OK)
	{
		fprintf(stderr, "Impossible de lire le fichier mp3\n");
		exit(EXIT_FAILURE);
	}
	

	/* On joue la musique */
	FMOD_System_PlaySound(system, FMOD_CHANNEL_FREE, musique, 0, NULL);

	FMOD_System_GetMasterChannelGroup(system, &channelgroup);
	FMOD_ChannelGroup_SetVolume(channelgroup, volume);
	
	FMOD_Sound_SetLoopCount(musique, -1);


	//////////////////////////////////
	/*------------ARDUINO-----------*/
	/////////////////////////////////
	
	/*--------------------------*/
	

	if (SP->IsConnected())
		printf("We're connected");
	
	//Sleep(500);
	
	
	/*BOMBE*/
	bombe.push_back(boum);
	
	tailleTableauBombe = bombe.size();

	Matrice = NULL;
	
	touch = false;
	depTime = false;
	start = false;
	gameOver = false;
	pause = false;
	win = false;
	page = 0;
	
	t = clock() / CLOCKS_PER_SEC;

	// Random d�placement ennemi //
	for (int i = 0; i < 4; i++) {
		r[i] = rand()% 4 + 1; 
	}

	//////////////////////////////////
	/*-------------LEVEL------------*/
	/////////////////////////////////
	level.niveau = 1;
	level.OuvrirNiveau("niveaux/niveau1.txt");
	level.Key();
	
	
	// Gestion de la fen�tre
	glutInitWindowPosition(0, 0);
	glutInitWindowSize(800, 800);
	glutInitDisplayMode(GLUT_RGBA | GLUT_SINGLE);
	glutCreateWindow("Bomberman");
	//glutFullScreen();

	glutSpecialFunc(traitementClavier);
	glutKeyboardFunc(traitementClavier2);
	glutTimerFunc(0, TraitementArduino, 0);
	

	///////////////////////////////////
	/*-----------TEXTURES-----------*/
	/////////////////////////////////

	/*Map*/
	LoadGLTextures("textures/level1/bloc_indestructible.png"); //0
	LoadGLTextures("textures/level2/Asteroide.png"); //1

	/*Player1*/
	LoadGLTextures("textures/level1/obiwan_right.png"); //2
	LoadGLTextures("textures/level1/obiwan_left.png"); //3
	LoadGLTextures("textures/level1/obiwan_top.png"); //4
	LoadGLTextures("textures/level1/obiwan_down.png"); //5

	/*Player2*/
	LoadGLTextures("textures/level2/FauconMillenium_Right.png"); //6
	LoadGLTextures("textures/level2/FauconMillenium_Left.png"); //7
	LoadGLTextures("textures/level2/FauconMillenium_Up.png"); //8
	LoadGLTextures("textures/level2/FauconMillenium_Down.png"); //9

	/*Player3*/
	LoadGLTextures("textures/level3/avatar_right.png"); //10
	LoadGLTextures("textures/level3/avatar_left.png"); //11
	LoadGLTextures("textures/level3/avatar_top.png"); //12
	LoadGLTextures("textures/level3/avatar_down.png"); //13

	/*Bombe*/
	LoadGLTextures("textures/level2/Bombe.png"); //14
	LoadGLTextures("textures/level2/Explosion_centre.png"); //15
	LoadGLTextures("textures/level2/Explosion_down.png"); //16
	LoadGLTextures("textures/level2/Explosion_left.png"); //17
	LoadGLTextures("textures/level2/Explosion_final_down.png"); //18
	LoadGLTextures("textures/level2/Explosion_final_top.png"); //19
	LoadGLTextures("textures/level2/Explosion_final_left.png"); //20
	LoadGLTextures("textures/level2/Explosion_final_right.png"); //21

	/*Briques*/
	LoadGLTextures("textures/level1/bloc.png"); //22
	LoadGLTextures("textures/level2/Asteroide_destructible.png"); //23
	LoadGLTextures("textures/level3/bloc_destructible.png"); //24
											   
	/*Ennemis*/
	LoadGLTextures("textures/level2/fighter.png"); //25 niveau 2

	/*Menu Level 1*/
	LoadGLTextures("textures/Accueil.png"); //26
	LoadGLTextures("textures/StartLevel1.png"); //27
	LoadGLTextures("textures/StartLevel2.png"); //28
	
	
	/*DOOR CLOSE*/
	LoadGLTextures("textures/level2/door_close.png"); //29

	/*BONUS Level 2*/
	LoadGLTextures("textures/level2/vitesse.png"); //30
	LoadGLTextures("textures/level2/bombe+.png"); //31
	LoadGLTextures("textures/level2/coeur.png"); //32
	LoadGLTextures("textures/level2/flame.png"); //33

	/*KEY*/
	LoadGLTextures("textures/key.png"); //34

	/*Ennemis Niveau 2*/
	LoadGLTextures("textures/level2/fighter_top.png"); //35 niveau 2
	LoadGLTextures("textures/level2/fighter_down.png"); //36 niveau 2
	LoadGLTextures("textures/level2/fighter_right.png"); //37 niveau 2
	LoadGLTextures("textures/level2/fighter_left.png"); //38 niveau 2
	LoadGLTextures("textures/level2/fighter_broken_top.png"); //39 niveau 2
	LoadGLTextures("textures/level2/fighter_broken_down.png"); //40 niveau 2
	LoadGLTextures("textures/level2/fighter_broken_right.png"); //41 niveau 2
	LoadGLTextures("textures/level2/fighter_broken_left.png"); //42 niveau 2

	/*DOOR OPEN*/
	LoadGLTextures("textures/level2/door_open.png"); //43

	/*Menu Level 3*/
	LoadGLTextures("textures/StartLevel3.png"); //44

	/*Win*/
	LoadGLTextures("textures/Win.png"); //45
	
	/*Menu Level 3*/
	LoadGLTextures("textures/Cinematique1_1.png"); //46
	LoadGLTextures("textures/Cinematique1_2.png"); //47

	/*Ennemis Niveau 1*/
	LoadGLTextures("textures/level1/jawa_down.png"); //48 niveau 2
	LoadGLTextures("textures/level1/jawa_top.png"); //49 niveau 2
	LoadGLTextures("textures/level1/jawa_right.png"); //50 niveau 2
	LoadGLTextures("textures/level1/jawa_left.png"); //51 niveau 2

	/*Ennemis Niveau 3*/
	LoadGLTextures("textures/level3/stormtrooper_top.png"); //52 niveau 3
	LoadGLTextures("textures/level3/stormtrooper_down.png"); //53 niveau 3
	LoadGLTextures("textures/level3/stormtrooper_right.png"); //54 niveau 3
	LoadGLTextures("textures/level3/stormtrooper_left.png"); //55 niveau 3
	LoadGLTextures("textures/level3/stormtrooper_down_touch.png"); //56 niveau 3
	LoadGLTextures("textures/level3/stormtrooper_right_touch.png"); //57 niveau 3
	LoadGLTextures("textures/level3/stormtrooper_top_touch.png"); //58 niveau 3
	LoadGLTextures("textures/level3/fighter_broken_left.png"); //59 niveau 3

	/*Background level 1*/
	LoadGLTextures("textures/level1/Background.png"); //60 niveau 2
	/*Background level 2*/
	LoadGLTextures("textures/level2/Background.png"); //61 niveau 2
	/*Background level 3*/
	LoadGLTextures("textures/level3/Background.png"); //62 niveau 2
	
	 /*BOMBE level 1*/
	LoadGLTextures("textures/level1/Bombe.png"); //63
	LoadGLTextures("textures/level1/Explosion_centre.png"); //64
	LoadGLTextures("textures/level1/Explosion_down.png"); //65
	LoadGLTextures("textures/level1/Explosion_left.png"); //66
	LoadGLTextures("textures/level1/Explosion_final_down.png"); //67
	LoadGLTextures("textures/level1/Explosion_final_top.png"); //68
	LoadGLTextures("textures/level1/Explosion_final_left.png"); //69
	LoadGLTextures("textures/level1/Explosion_final_right.png"); //70

	/*BONUS Level 1*/
	LoadGLTextures("textures/level1/vitesse.png"); //71
	LoadGLTextures("textures/level1/bombe+.png"); //72
	LoadGLTextures("textures/level1/coeur.png"); //73
	LoadGLTextures("textures/level1/flame.png"); //74
													  
	/*Player1 TOUCH*/
	
	LoadGLTextures("textures/level1/obiwan_down_touch.png"); //75
	LoadGLTextures("textures/level1/obiwan_right_touch.png"); //76
	LoadGLTextures("textures/level1/obiwan_top_touch.png"); //77

	/*Player2 TOUCH*/

	LoadGLTextures("textures/level2/FauconMillenium_Right_touch.png"); //78
	LoadGLTextures("textures/level2/FauconMillenium_Left_touch.png"); //79
	LoadGLTextures("textures/level2/FauconMillenium_Up_touch.png"); //80
	LoadGLTextures("textures/level2/FauconMillenium_Down_touch.png"); //81

	/*Player3 TOUCH*/

	LoadGLTextures("textures/level3/avatar_down_touch.png"); //82
	LoadGLTextures("textures/level3/avatar_right_touch.png"); //83
	LoadGLTextures("textures/level3/avatar_top_touch.png"); //84

	LoadGLTextures("textures/boum.png"); //85
	LoadGLTextures("textures/pause.png"); //86

	/*BOMBE level 3*/
	LoadGLTextures("textures/level3/Bombe.png"); //87
	LoadGLTextures("textures/level3/Explosion_centre.png"); //88
	LoadGLTextures("textures/level3/Explosion_down.png"); //89
	LoadGLTextures("textures/level3/Explosion_left.png"); //90
	LoadGLTextures("textures/level3/Explosion_final_down.png"); //91
	LoadGLTextures("textures/level3/Explosion_final_top.png"); //92
	LoadGLTextures("textures/level3/Explosion_final_left.png"); //93
	LoadGLTextures("textures/level3/Explosion_final_right.png"); //94

	/*PORTE BLOC LEVEL 3*/
	LoadGLTextures("textures/level3/porte_bloc.png"); //95		
	LoadGLTextures("textures/level3/porte_bloc2.png"); //96
	
	/*MUR LEVEL 3*/
	LoadGLTextures("textures/level3/bloc_indestructible.png"); //97			

	/*FAILED LEVEL*/
	LoadGLTextures("textures/FailedLevel_restart.png"); //98	
	LoadGLTextures("textures/FailedLevel_quit.png"); //99
													  
													  
	// Gestion des �v�nement
	
	glutTimerFunc(1000, myTimer, 1);
	glutDisplayFunc(LabyAffichage);
	glutReshapeFunc(LabyRedim);

	glutMainLoop();

	

	return 0;
}







