/////////////////////////////////////////////////////////////
//														   //
//				PROJET BOMBERMAN						   //
//														   //
//				----- Class Timer -----				       //
//														   //
/////////////////////////////////////////////////////////////

#include "Timer.h"


Timer::Timer()
{
	startedAt = 0;
	pausedAt = 0;
	paused = false;
	started = false;
}

bool Timer::IsStarted()
{
	return started;
}
bool Timer::IsStopped()
{
	return !started;
}
bool Timer::IsPaused()
{
	return paused;
}
bool Timer::IsActive()
{
	return !paused & started;
}
void Timer::Pause()
{
	if (paused || !started)
		return;
	paused = true;
	pausedAt =  clock() / CLOCKS_PER_SEC;
}
void Timer::Resume()
{
	if (!paused)
		return;
	paused = false;
	startedAt += (clock() / CLOCKS_PER_SEC) -  pausedAt;
}
void Timer::Stop()
{
	started = false;
}
void Timer::Start()
{
	if (started)
		return;
	started = true;
	paused = false;
	startedAt = 120;
}
void Timer::Reset()
{
	paused = false;
	startedAt = clock();
	
}
clock_t Timer::GetTicks()
{
	if (!started)
		return 0;
	if (paused)
		return startedAt - pausedAt ;
	return  startedAt-clock();
}

clock_t Timer::Temps()
{
	if (!started)
		return 0;
	if (paused)
		return startedAt - pausedAt;
	return  startedAt - clock()/ CLOCKS_PER_SEC;
}

Timer::~Timer()
{
}
