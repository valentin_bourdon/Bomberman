/////////////////////////////////////////////////////////////
//														   //
//					PROJET Bomberman					   //
//														   //
//				----- Class Ennemi -----				   //
//														   //
/////////////////////////////////////////////////////////////

#include "ennemi.h"
#include "Niveau.h"

using namespace std;

extern char** Matrice;


extern int NbColonnes;
extern int NbLignes;

extern Niveau level;

extern int r[4]; // On r�cup�re le tableau d'entier qui sert pour le d�placements al�atoire des ennemis

extern vector<GLuint> texture; // On r�cup�re les textures pour afficher une texture diff�rente � chaque ennemi



ennemi::ennemi()
{
	x = 0;
	y = 0;	
	img = 38;
	nbVies = 0;
	offsetX = 0.0;
	offsetY = 0.0;

	depH = false;
	depB = false;
	depG = false;
	depD = false;

	touch = false;
	
}



int ennemi::PositionX()
{
	return x;
}

int  ennemi::PositionY()
{
	return y;
}

void ennemi::ResetPos(int abs, int ord, int vie)
{
	x = abs;
	y = ord;
	offsetX = 0.0;
	offsetY = 0.0;
	nbVies = vie;
	depH = false;
	depB = false;
	depG = false;
	depD = false;

	touch = false;
}



void ennemi::UpdateVie()
{
	nbVies--;
}

int ennemi::ReturnVie()
{
	return nbVies;
}

void ennemi::ResetVie(int _vie)
{
	nbVies = _vie;
}


// D�placement de l'�nnemi //
void ennemi::DepGauche(int i)
{	
	
	if (level.niveau == 1) {
		if (touch == true)img =85;
		else img = 50;
	}
	else if (level.niveau == 2) {
		if (touch == true)img =85;
		else if (ReturnVie() == 1)img = 41;
		else img = 38;
		
			
	}
	else if (level.niveau == 3) {
		if (touch == true)img = 85;
		else if (ReturnVie() == 1)img = 56;
		else img = 55;
	}

	
	depG = true;

	if((Matrice[y][x - 1] != '1' || Matrice[y][x - 1] != '2' || Matrice[y][x - 1] != '�' || Matrice[y][x - 1] != '~' || Matrice[y][x - 1] != '+' || Matrice[y][x - 1] != '*' || Matrice[y][x - 1] != '/' || Matrice[y][x - 1] != '&')&&(offsetY<0.1 && offsetY>-0.1)) {
		offsetX -= 0.1;
		offsetY = 0.0;
		if (offsetX < -0.95) {
			offsetX = 0.0;
			x--;
			depG = false;
			depD = false;
			depH = false;
			depB = false;
		}
	}
	

	
}
void ennemi::DepDroite(int i)
{
	
	if (level.niveau == 1) {
		if (touch == true)img =85 ;
		else img = 51;
	}
	else if (level.niveau == 2) {
		if (touch == true)img =85 ;
		else if (ReturnVie() == 1)img = 42;
		else img = 37;
	}
	else if (level.niveau == 3) {
		if (touch == true)img =85 ;
		else if (ReturnVie() == 1) img = 57;
		else img = 54;
	}
	

	
	depD = true;

	if ((Matrice[y][x + 1] != '1' || Matrice[y][x + 1] != '2' || Matrice[y][x + 1] != '�' ||  Matrice[y][x + 1] != '~' || Matrice[y][x + 1] != '+' || Matrice[y][x + 1] != '*' || Matrice[y][x + 1] != '/' || Matrice[y][x + 1] != '&') && (offsetY<0.1 && offsetY>-0.1)) {
		offsetX += 0.1;
		offsetY = 0.0;
		if (offsetX > 0.95) {
			offsetX = 0.0;
			x++;
			depG = false;
			depD = false;
			depH = false;
			depB = false;
		}
	}
	
	
}
void ennemi::DepTop(int i)
{

	if (level.niveau == 1) {
		if (touch == true)img =85 ;
		else img = 49;
	}
	else if (level.niveau == 2) {
		if (touch == true)img =85 ;
		else if (ReturnVie() == 1)img = 39;
		else img = 35;
	}
	else if (level.niveau == 3) {
		if (touch == true)img =85 ;
		else if (ReturnVie() == 1) img = 58;
		else img = 52;
	}
	
	depH = true;
	if ((Matrice[y - 1][x] != '1' || Matrice[y - 1][x] != '2' || Matrice[y - 1][x] != '�' || Matrice[y - 1][x] != '~' || Matrice[y - 1][x] != '+' || Matrice[y - 1][x] != '*' || Matrice[y - 1][x] != '/' || Matrice[y - 1][x] != '&')&&(offsetX<0.1 && offsetX>-0.1)) {
		offsetY -= 0.1;
		offsetX = 0.0;
		if (offsetY < -0.95) {
			offsetY = 0.0;
			y--;
			depG = false;
			depD = false;
			depH = false;
			depB = false;
		}
	}
	
	
}
void ennemi::DepBottom(int i)
{

	
	if (level.niveau == 1) {
		if (touch == true)img =85 ;
		else img = 48;
	}
	else if (level.niveau == 2) {
		if (touch == true)img =85 ;
		else if (ReturnVie() == 1)img = 40;
		else img = 36;
	}
	else if (level.niveau == 3) {
		if (touch == true)img =85 ;
		else if (ReturnVie() == 1) img = 56;
		else img = 53;
	}
	
	depB = true;
	
	if ((Matrice[y + 1][x] != '1' || Matrice[y + 1][x] != '2' || Matrice[y + 1][x] != '�' || Matrice[y + 1][x] != '~' || Matrice[y + 1][x] != '+' || Matrice[y + 1][x] != '*' || Matrice[y + 1][x] != '/'|| Matrice[y + 1][x] != '&') && (offsetX<0.1 && offsetX>-0.1)) {
		offsetY += 0.1;
		offsetX = 0.0;
		if (offsetY > 0.95) {
			offsetY = 0.0;
			y++;
			depG = false;
			depD = false;
			depH = false;
			depB = false;
		}
	}
	

}



//Affichage de l'ennemi //
void ennemi::Dessiner()
{


	if (touch == true)img = 85;

	glPushMatrix();

	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glBindTexture(GL_TEXTURE_2D, texture[img]);
	glBegin(GL_QUADS);
	glColor4d(1.0, 1.0, 1.0, 1.0);
	glTexCoord2f(1.0f,1.0f); glVertex2d(x + offsetX, y + offsetY);
	glTexCoord2f(0.0f, 1.0f); glVertex2d(x + 1 + offsetX, y + offsetY);
	glTexCoord2f(0.0f, 0.0f); glVertex2d(x + 1 + offsetX, y + 1 + offsetY);
	glTexCoord2f(1.0f, 0.0f); glVertex2d(x + offsetX, y + 1 + offsetY);
	glEnd();
	glDisable(GL_TEXTURE_2D);

	glPopMatrix();
	glutPostRedisplay();

}

// D�placement all�atoire de l'ennemi //
void ennemi::DeplacementEnnemi(int abs, int ord, int i, int _vitesse)
{
		x = abs;
		y = ord;

	//DEPLACEMENT DROITE//  
	if (r[i] == 1) {
		if ((Matrice[y - 1][x] != '0') && (Matrice[y + 1][x] != '0') && (Matrice[y][x + 1] == '0')) { //DROITE    
			DepDroite(i); 
			r[i] = 1;	
		}
		else if ((Matrice[y + 1][x] != '0') && (Matrice[y][x + 1]  == '0' ) && (Matrice[y - 1][x] == '0') ) { //DROITE + HAUT
			r[i] = rand() % 3 + 1;
			if (r[i] == 1) {
				DepDroite(i); 
				r[i] = 1;
			}
			else {
				DepTop(i); 
				r[i] = 2;
			}
		}
		else if ((Matrice[y - 1][x] != '0') && (Matrice[y + 1][x]  == '0') && (Matrice[y][x + 1]  == '0' )) { //DROITE + BAS

			do {
				r[i] = rand() % 4 + 1;

			} while (r[i] == 2);

			if (r[i] == 1) {
				DepDroite(i); 
				r[i] = 1;
			}
			else {
				DepBottom(i);
				r[i] = 3;
			}

		}
		else if ((Matrice[y - 1][x]  == '0') && (Matrice[y + 1][x]  == '0') && (Matrice[y][x + 1]  == '0' )) { //HAUT + BAS + DROITE
			r[i] = rand() % 4 + 1;
			if (r[i] == 1) {
				DepDroite(i); 
				r[i] = 1;
			}
			else if (r[i] == 2) {
				DepTop(i); 
				r[i] = 2;
			}
			else {
				DepBottom(i);
				r[i] = 3;
			}

		}
		else if ((Matrice[y - 1][x]  == '0') && (Matrice[y + 1][x]  == '0') && (Matrice[y][x + 1] != '0')) { //HAUT + BAS
			do {
				r[i] = rand() % 4 + 1;
			} while (r[i] == 1);

			if (r[i] == 2) {
				
				DepTop(i); 
				r[i] = 2;
			}
			else {
				
				DepBottom(i);
				r[i] = 3;
			}

		}
		else if ((Matrice[y - 1][x]  == '0') && (Matrice[y + 1][x] != '0') && (Matrice[y][x + 1] != '0')) { //HAUT
			
			DepTop(i); 
			r[i] = 2;
		}
		else if ((Matrice[y - 1][x] != '0') && (Matrice[y + 1][x]  == '0') && (Matrice[y][x + 1] != '0')) { //BAS
			
			DepBottom(i);
			r[i] = 3;

		}
		else if ((Matrice[y-1][x] != '0') && (Matrice[y+1][x]!='0') && (Matrice[y][x+1]!='0') && (Matrice[y][x - 1] == '0') && offsetX == 0.0 && offsetY == 0.0) { //Retour en arri�re (gauche)
			DepGauche(i); 
			r[i] = 4;
		}
		

	}

	//DEPLACEMENT EN HAUT//

	else if (r[i] == 2) {
		if ((Matrice[y - 1][x]  == '0') && (Matrice[y][x + 1] != '0') && (Matrice[y][x - 1] != '0')) { //HAUT
			
			DepTop(i); 
			r[i] = 2;
			
		}
		else if ((Matrice[y - 1][x]  == '0') && (Matrice[y][x + 1]  == '0' ) && (Matrice[y][x - 1] != '0')) { //HAUT + DROITE
			r[i] = rand() % 3 + 1;
			
			if (r[i] == 1) {
				
				DepDroite(i); 
				r[i] = 1;
				
			}
			else {
				
				DepTop(i); 
				r[i] = 2;
				
			}
		}
		else if ((Matrice[y - 1][x]  == '0') && (Matrice[y][x + 1] != '0') && (Matrice[y][x - 1]  == '0')) { //HAUT + GAUCHE
			do {
				r[i] = rand() % 4 + 1;
			} while (r[i] == 1 || r[i] == 3);

			if (r[i] == 2) {
				
				DepTop(i); 
				r[i] = 2;
				
			}
			else {
				
				DepGauche(i); 
				r[i] = 4;
				
			}
		}
		else if ((Matrice[y - 1][x] != '0') && (Matrice[y][x + 1]  == '0' ) && (Matrice[y][x - 1]  == '0')) { // GAUCHE + DROITE
			do {
				r[i] = rand() % 4 + 1;
			} while (r[i] == 2 || r[i] == 3);

			if (r[i] == 1) {
				DepDroite(i); 
				r[i] = 1;
				
			}
			else {
				DepGauche(i); 
				r[i] = 4;
				
			}
		}
		else if ((Matrice[y - 1][x] != '0') && (Matrice[y][x + 1] != '0') && (Matrice[y][x - 1]  == '0')) { //GAUCHE

			DepGauche(i); 
			r[i] = 4;
			
		}
		else if ((Matrice[y - 1][x] !='0') && (Matrice[y][x + 1]  =='0' ) && (Matrice[y][x - 1] !='0')) { //DROITE
			DepDroite(i); 
			r[i] = 1;
			
		}
		else if ((Matrice[y - 1][x]  =='0') && (Matrice[y][x + 1]  =='0' ) && (Matrice[y][x - 1]  =='0')) { //DROITE + GAUCHE + HAUT
			do {
				r[i] = rand() % 4 + 1;
			} while (r[i] == 3);

			if (r[i] == 1) {
				DepDroite(i); 
				r[i] = 1;
				
			}
			else if (r[i] == 2) {
				DepTop(i); 
				r[i] = 2;
				
			}
			else {
				DepGauche(i); 
				r[i] = 4;
				
			}
		}
		else if ((Matrice[y][x+1]!='0' && Matrice[y][x-1] !='0' && Matrice[y-1][x]!='0') && offsetX == 0.0 && offsetY == 0.0) { //retour en arri�re (bas)
			DepBottom(i);
			r[i] = 3;
			
		}

	}


	//DEPLACEMENT GAUCHE//

	else if (r[i] == 4) {
		if ((Matrice[y - 1][x] !='0') && (Matrice[y + 1][x] !='0') && (Matrice[y][x - 1]  =='0')) { //GAUCHE
			DepGauche(i); 
			r[i] = 4;
			
		}
		else if ((Matrice[y - 1][x]  =='0') && (Matrice[y + 1][x] !='0') && (Matrice[y][x - 1]  =='0')) { //GAUCHE + HAUT
			do {
				r[i] = rand() % 4 + 1;
			} while (r[i] == 1 || r[i] == 3);

			if (r[i] == 4) {
				DepGauche(i); 
				r[i] = 4;
				
			}
			else {
				DepTop(i); 
				r[i] = 2;
				
			}
		}
		else if ((Matrice[y - 1][x] !='0') && (Matrice[y + 1][x]  =='0') && (Matrice[y][x - 1]  =='0')) { //GAUCHE + BAS
			do {
				r[i] = rand() % 4 + 1;
			} while (r[i] == 1 || r[i] == 2);

			if (r[i] == 4) {
				DepGauche(i); 
				r[i] = 4;
				
			}
			else {
				DepBottom(i);
				r[i] = 3;
				
			}
		}
		else if ((Matrice[y - 1][x]  =='0') && (Matrice[y + 1][x]  =='0') && (Matrice[y][x - 1] !='0')) { //HAUT + BAS
			do {
				r[i] = rand() % 4 + 1;
			} while (r[i] == 1);

			if (r[i] == 2) {
				DepTop(i); 
				r[i] = 2;
				
			}
			else {
				DepBottom(i);
				r[i] = 3;
				
			}
		}
		else if ((Matrice[y - 1][x]  =='0') && (Matrice[y + 1][x] !='0') && (Matrice[y][x - 1] !='0')) { //HAUT

			DepTop(i); 
			r[i] = 2;
			
		}
		else if ((Matrice[y - 1][x] !='0') && (Matrice[y + 1][x]  =='0') && (Matrice[y][x - 1] !='0')) { //BAS

			DepBottom(i);
			r[i] = 3;
			

		}
		else if ((Matrice[y - 1][x]  =='0') && (Matrice[y + 1][x]  =='0') && (Matrice[y][x - 1]  =='0')) { //BAS+HAUT+GAUCHE
			do {
				r[i] = rand() % 4 + 1;
			} while (r[i] == 1);

			if (r[i] == 2) {
				DepTop(i);
				r[i] = 2;
				
			}
			else if (r[i] == 3) {
				DepBottom(i);
				r[i] = 3;
				
			}
			else {
				DepGauche(i); 
				r[i] = 4;
				
			}

		}
		else if ((Matrice[y-1][x]!='0') && (Matrice[y+1][x]!='0') && (Matrice[y][x-1]!='0') && (Matrice[y][x + 1] == '0') && offsetX == 0.0 && offsetY == 0.0) { //retour en arri�re (droite)
			DepDroite(i);
			r[i] = 1;
			
		}

	}


	//DEPLACEMENT BAS //

	else if (r[i] == 3) {
		if ((Matrice[y + 1][x]  =='0') && (Matrice[y][x - 1] !='0') && (Matrice[y][x + 1] !='0')) { //BAS
			DepBottom(i);
			r[i] = 3;
			
		}
		else if ((Matrice[y + 1][x]  =='0') && (Matrice[y][x - 1] !='0') && (Matrice[y][x + 1]  =='0' )) { //BAS + DROITE
			do {
				r[i] = rand() % 4 + 1;
			} while (r[i] == 2);
		
			
			if (r[i] == 1) {
				DepDroite(i); 
				r[i] = 1;
				
			}
			else {
				DepBottom(i);
				r[i] = 3;
				
			}
		}
		else if ((Matrice[y + 1][x]  =='0') && (Matrice[y][x - 1]  =='0') && (Matrice[y][x + 1] !='0')) { //BAS + GAUCHE
			do {
				r[i] = rand() % 4 + 1;
			} while (r[i] == 1 || r[i] == 2);

			if (r[i] == 3) {
				DepBottom(i);
				r[i] = 3;
				
			}
			else {
				DepGauche(i); 
				r[i] = 4;
				
			}
		}
		else if ((Matrice[y + 1][x] !='0') && (Matrice[y][x - 1]  =='0' ) && (Matrice[y][x + 1]  =='0' )) { //DROITE + GAUCHE
			do {
				r[i] = rand() % 4 + 1;
			} while (r[i] == 2 || r[i] == 3);

			if (r[i] == 1) {
				DepDroite(i); 
				r[i] = 1;
				
			}
			else {
				DepGauche(i); 
				r[i] = 4;
				
			}
		}
		else if ((Matrice[y + 1][x] !='0') && (Matrice[y][x - 1]  =='0') && (Matrice[y][x + 1] !='0')) { //GAUCHE
			DepGauche(i);
			r[i] = 4;
			

		}
		else if ((Matrice[y + 1][x] !='0') && (Matrice[y][x - 1] !='0') && (Matrice[y][x + 1]  =='0' )) { //DROITE
			DepDroite(i); 
			r[i] = 1;
			

		}
		else if ((Matrice[y + 1][x]  =='0') && (Matrice[y][x - 1]  =='0') && (Matrice[y][x + 1]  =='0' )) { //BAS + GAUCHE + DROITE
			do {
				r[i] = rand() % 4 + 1;
			} while (r[i] == 2);

			if (r[i] == 1) {
				DepDroite(i); 
				r[i] = 1;
				
			}
			else if (r[i] == 3) {
				DepBottom(i);
				r[i] = 3;
				
			}
			else {
				DepGauche(i);
				r[i] = 4;
				
			}
		}
		else if ((Matrice[y][x+1]!='0') && (Matrice[y][x-1]!='0') && (Matrice[y+1][x]!='0') && offsetX == 0.0 && offsetY == 0.0) { //retour en arri�re (haut)
			DepTop(i);
			r[i] = 2;
			
		}

	}

	



}


