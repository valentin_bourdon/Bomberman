/////////////////////////////////////////////////////////////
//														   //
//					PROJET BOMBERMAN				       //
//														   //
//				----- Class Bombe -----				       //
//														   //
/////////////////////////////////////////////////////////////

#include "Bombe.h"
#include "Niveau.h"
#include "Bonus.h"
#include "Timer.h"
#include "avatar.h"

using namespace std;

extern char** Matrice;

extern int NbColonnes=11;
extern int NbLignes=11;

extern int tailleTableauBombe;
extern void timerBombeExplose(int value);
extern void timerPoseBombe(int value);


extern Niveau level;
extern Timer aTimer;
extern avatar player;

extern vector<GLuint> texture; 

extern Bonus bonnus;

Bombe::Bombe()
{
	
	startTime = aTimer.Temps();
	x = player.PositionX();
	y = player.PositionY();
	distance = 1;
	explose = false;
	pose = false;
	d = false;
	g = false;
	h = false;
	b = false;

	murD = false;
	murG = false;
	murH = false;
	murB = false;

	timer = false;

	touchBombe = false;

	
	

}



void Bombe::UpdatePosition(int abs, int ord)
{
	x = abs;
	y = ord;
}

int Bombe::PositionX()
{
	return x;
}

int Bombe::PositionY()
{
	return y;
}


void Bombe::Destroy()
{

	pose = false;

		//Lorsque la bombe explose on change la matrice
	if (explose == true) {
			
		
		if (touchBombe == true) {
			Matrice[y][x] = '^';
		}
		
		if (TouchBas() == true || TouchHaut() == true || TouchDroite() == true || TouchGauche() == true) {
			for (int i = 1; i <= distance; i++) {

				/*--DROITE--*/
				if ((x + i < NbLignes) && (ExploseDroite()) && (d == false) && (murD == false) && (Matrice[y][x + i] != '|') && (Matrice[y][x + i] != '@')) {

					if ((Matrice[y][x + i] != '1') && (Matrice[y][x + i] != '2') && (Matrice[y][x + i] != '#') && (Matrice[y][x + i] != '~') && (Matrice[y][x + i] != '�')) {	  //droite
						if ((i == distance) && (Matrice[y][x + i] != '^')) {
							Matrice[y][x + i] = '4';
						}
						else Matrice[y][x + i] = '3';
					}
					else if (Matrice[y][x + i] == '2') {
						Matrice[y][x + i] = '4';
						d = true;

					}
					else if ((Matrice[y][x + i] == '1') || (Matrice[y][x + i] == '~') || (Matrice[y][x + i] == '�')) { //un mur 
						Matrice[y][x + i - 1] = '4';
						murD = true;

					}
					else if (Matrice[y][x + i] == '#') { //une bombe
						Matrice[y][x + i] = '^';
						cout << "touch";
						murD = true;
						touchBombe = true;
					}
				}
				/*-------------------*/
				/*--GAUCHE--*/
				if ((x - i >= 0) && ExploseGauche() && (g == false) && (murG == false) && (Matrice[y][x - i] != '|') && (Matrice[y][x - i] != '@')) {

					if ((Matrice[y][x - i] != '1') && (Matrice[y][x - i] != '2') && (Matrice[y][x - i] != '#') && (Matrice[y][x - i] != '~') && (Matrice[y][x - i] != '�')) { //gauche
						if (i == distance && (Matrice[y][x - i] != '^')) {
							Matrice[y][x - i] = '5';

						}
						else Matrice[y][x - i] = '3';
					}
					else if (Matrice[y][x - i] == '2') {
						Matrice[y][x - i] = '5';
						g = true;

					}
					else if ((Matrice[y][x - i] == '1') || (Matrice[y][x - i] == '~') || (Matrice[y][x - i] == '�')) { //un mur ou une porte de sortie
						Matrice[y][x - i + 1] = '5';
						murG = true;

					}
					else if (Matrice[y][x - i] == '#') { //une bombe
						Matrice[y][x - i] = '^';
						cout << "touch";
						murG = true;
						touchBombe = true;


					}
				}
				/*-------------------*/
				/*--BAS--*/
				if ((y + i < NbColonnes) && ExploseBas() && (b == false) && (murB == false) && (Matrice[y + i][x] != '|') && (Matrice[y + i][x] != '@') ) {

					if ((Matrice[y + i][x] != '1') && (Matrice[y + i][x] != '2') && (Matrice[y + i][x] != '#') && (Matrice[y + i][x] != '~') && (Matrice[y + i][x] != '�')) { //bas
						if (i == distance && (Matrice[y + i][x] != '^')) {
							Matrice[y + i][x] = '8';

						}
						else Matrice[y + i][x] = '6';
					}
					else if (Matrice[y + i][x] == '2') {
						Matrice[y + i][x] = '8';  //derni�re texture
						b = true;

					}
					else if ((Matrice[y + i][x] == '1') || (Matrice[y + i][x] == '~') || (Matrice[y + i][x] == '�')) { //un mur ou une porte de sortie
						Matrice[y + i - 1][x] = '8';
						murB = true;

					}
					else if (Matrice[y + i][x] == '#') { //une bombe
						Matrice[y + i][x] = '^';
						cout << "touch";
						murB = true;
						touchBombe = true;


					}
				}
				/*-------------------*/
				/*--HAUT--*/
				if ((y - i >= 0) && ExploseHaut() && (h == false) && (murH == false) && (Matrice[y - i][x] != '|') && (Matrice[y - i][x] != '@') ) {

					if ((Matrice[y - i][x] != '1') && (Matrice[y - i][x] != '2') && (Matrice[y - i][x] != '#') && (Matrice[y - i][x] != '~') && (Matrice[y - i][x] != '�')) { //haut
						if (i == distance && (Matrice[y - i][x] != '^')) {
							Matrice[y - i][x] = '7';

						}
						else Matrice[y - i][x] = '6';
					}
					else if ((Matrice[y - i][x] == '2')) {
						Matrice[y - i][x] = '7';
						h = true;

					}
					else if ((Matrice[y - i][x] == '1') || (Matrice[y - i][x] == '~') || (Matrice[y - i][x] == '�')) {
						Matrice[y - i + 1][x] = '7';
						murH = true;

					}
					else if (Matrice[y - i][x] == '#') { //une bombe
						Matrice[y - i][x] = '^';
						cout << "touch";
						murH = true;
						touchBombe = true;


					}
				}
			}
		}
		else {
			Matrice[y][x] = '^';
			for (int i = 1; i <= distance; i++) {

				/*--DROITE--*/
				if ((x + i < NbLignes) && (ExploseDroite()) && (d == false) && (murD == false) && (Matrice[y][x + i] != '|') && (Matrice[y][x + i] != '@') ) {

					if ((Matrice[y][x + i] != '1') && (Matrice[y][x + i] != '2') && (Matrice[y][x + i] != '#') && (Matrice[y][x + i] != '~') && (Matrice[y][x + i] != '�')) {	  //droite
						if ((i == distance) && (Matrice[y][x + i] != '^')) {
							Matrice[y][x + i] = '4';
						}
						else Matrice[y][x + i] = '3';
					}
					else if (Matrice[y][x + i] == '2') {
						Matrice[y][x + i] = '4';
						d = true;

					}
					else if ((Matrice[y][x + i] == '1') || (Matrice[y][x + i] == '~') || (Matrice[y][x + i] == '�')) { //un mur 
						Matrice[y][x + i - 1] = '4';
						murD = true;

					}
				}
				/*-------------------*/
				/*--GAUCHE--*/
				if ((x - i >= 0) && ExploseGauche() && (g == false) && (murG == false) && (Matrice[y][x - i] != '|') && (Matrice[y][x - i] != '@') ) {

					if ((Matrice[y][x - i] != '1') && (Matrice[y][x - i] != '2') && (Matrice[y][x - i] != '#') && (Matrice[y][x - i] != '~') && (Matrice[y][x - i] != '�')) { //gauche
						if (i == distance && (Matrice[y][x - i] != '^')) {
							Matrice[y][x - i] = '5';

						}
						else Matrice[y][x - i] = '3';
					}
					else if (Matrice[y][x - i] == '2') {
						Matrice[y][x - i] = '5';
						g = true;

					}
					else if ((Matrice[y][x - i] == '1') || (Matrice[y][x - i] == '~') || (Matrice[y][x - i] == '�')) { //un mur ou une porte de sortie
						Matrice[y][x - i + 1] = '5';
						murG = true;

					}

				}
				/*-------------------*/
				/*--BAS--*/
				if ((y + i < NbColonnes) && ExploseBas() && (b == false) && (murB == false) && (Matrice[y + i][x] != '|') && (Matrice[y + i][x] != '@')) {

					if ((Matrice[y + i][x] != '1') && (Matrice[y + i][x] != '2') && (Matrice[y + i][x] != '#') && (Matrice[y + i][x] != '~') && (Matrice[y + i][x] != '�')) { //bas
						if (i == distance && (Matrice[y + i][x] != '^')) {
							Matrice[y + i][x] = '8';

						}
						else Matrice[y + i][x] = '6';
					}
					else if (Matrice[y + i][x] == '2') {
						Matrice[y + i][x] = '8';  //derni�re texture
						b = true;

					}
					else if ((Matrice[y + i][x] == '1') || (Matrice[y + i][x] == '~') || (Matrice[y + i][x] == '�')) { //un mur ou une porte de sortie
						Matrice[y + i - 1][x] = '8';
						murB = true;

					}

				}
				/*-------------------*/
				/*--HAUT--*/
				if ((y - i >= 0) && ExploseHaut() && (h == false) && (murH == false) && (Matrice[y - i][x] != '|') && (Matrice[y - i][x] != '@') ) {

					if ((Matrice[y - i][x] != '1') && (Matrice[y - i][x] != '2') && (Matrice[y - i][x] != '#') && (Matrice[y - i][x] != '~') && (Matrice[y - i][x] != '�')) { //haut
						if (i == distance && (Matrice[y - i][x] != '^')) {
							Matrice[y - i][x] = '7';

						}
						else Matrice[y - i][x] = '6';
					}
					else if ((Matrice[y - i][x] == '2')) {
						Matrice[y - i][x] = '7';
						h = true;

					}
					else if ((Matrice[y - i][x] == '1') || (Matrice[y - i][x] == '~') || (Matrice[y - i][x] == '�')) {
						Matrice[y - i + 1][x] = '7';
						murH = true;

					}

				}
			}
		}
			
	}

		//On remet les valeurs de la matrice a '0' quand la bombe � explos� ou on affiche un bonus suivant le random
	if (explose == false) {
			
			//cout << "SUPERIEUR";
		touchBombe = false; 
		
			Matrice[y][x] = '0'; //position de la bombe

			for (int i = 1; i <= distance; i++) {

				/*--DROITE--*/
				if (x + i < NbLignes) {
					murD = false;
					if (Matrice[y][x + i] == '3') {	  //si la matrice vaut 3 (explosion bombe)
						Matrice[y][x + i] = '0';  //case vide
						
					}
					else if (Matrice[y][x + i] == '4'  && d == false) { //mur destructible non touch� et matrice vaut 6 (texture derni�re explosion bombe)
						Matrice[y][x + i] = '0';  //case vide
					}
					else if (Matrice[y][x + i] == '4' && d == true) {  //mur destructible touch� et matrice vaut 6 (texture derni�re explosion bombe)
						if ((y == level.keyY) && (x + i == level.keyX)) {
							Matrice[y][x + i] = '@';  //case avec de la cl�
							d = false;
						}
						else {
							srand(time(NULL));
							int r = rand() % 10 + 1;  //valeur al�atoire pour mettre un bonus ou non 
							cout << "droite : " << r << "\n";
							if (r == 1) {
								Matrice[y][x + i] = '0';  //case vide
								d = false;
							}
							else if (r == 2) {

								Matrice[y][x + i] = '+';  //case avec un bonus +nbBombe
								d = false;
							}
							else if (r == 3) {

								Matrice[y][x + i] = '*'; //case avec un bonus +vitesse
								d = false;
							}
							else if (r == 4) {

								Matrice[y][x + i] = '/'; //case avec un bonus +distance
								d = false;
								
							}
							else if ((r == 5) && player.ReturnVie() < 3) {

								Matrice[y][x + i] = '&'; //case avec un bonus +vie
								d = false;
							}
							else {
								Matrice[y][x + i] = '0';  //case vide
								d = false;
							}
						}
						
						
					}

				}
				/*-----------------*/
				/*--GAUCHE--*/
				if (x - i > 0) {
					murG = false;
					if (Matrice[y][x - i] == '3') { //gauche
						Matrice[y][x - i] = '0';  //case vide
					}
					else if (Matrice[y][x - i] == '5' && g == false) {
						Matrice[y][x - i] = '0';  //case vide
					}
					else if (Matrice[y][x - i] == '5' && g == true) {
						if (y == level.keyY && x - i == level.keyX) {
							Matrice[y][x - i] = '@';  //case avec un bonus +nbBombe
							g = false;
						}
						else {
							srand(time(NULL));
							int r = rand() % 10 + 1;  //valeur al�atoire pour mettre un bonus ou non 
							cout << "gauche : " << r << "\n";
							if (r == 1) {
								Matrice[y][x - i] = '0';  //case vide
								g = false;

							}
							else if (r == 2) {

								Matrice[y][x - i] = '+'; //case avec un bonus +nbBombe
								g = false;

							}
							else if (r == 3) {

								Matrice[y][x - i] = '*'; //case avec un bonus +vitesse
								g = false;

							}
							else if (r == 4) {

								Matrice[y][x - i] = '/'; //case avec un bonus +distance
								g = false;

							}
							else if ((r == 5) && player.ReturnVie() < 3) {

								Matrice[y][x - i] = '&'; //case avec un bonus +vie
								g = false;

							}
							else {
								Matrice[y][x - i] = '0';  //case vide
								g = false;
							}
						}
						
					}

				}
				/*-----------------*/
				/*--BAS--*/
				if ((y + i < NbColonnes)) {
					murB = false;
					if (Matrice[y + i][x] == '6') { //bas
						Matrice[y + i][x] = '0'; //case vide
					}
					else if (Matrice[y + i][x] == '8' && b == false) {
						Matrice[y + i][x] = '0'; //case vide
					}
					else if (Matrice[y + i][x] == '8' && b == true) {
						if (y+i == level.keyY && x == level.keyX) {
							Matrice[y+i][x] = '@';  //case avec un bonus +nbBombe
							b = false;
						}
						else {
							srand(time(NULL));
							int r = rand() % 10 + 1;  //valeur al�atoire pour mettre un bonus ou non 
							cout << "bas : " << r << "\n";
							if (r == 1) {
								Matrice[y + i][x] = '0';  //case vide
								b = false;
							}
							else if (r == 2) {

								Matrice[y + i][x] = '+';  //case avec un bonus +nbBombe
								b = false;
							}
							else if (r == 3) {

								Matrice[y + i][x] = '*'; //case avec un bonus +vitesse
								b = false;
							}
							else if (r == 4) {

								Matrice[y + i][x] = '/'; //case avec bonus +distance
								b = false;

							}
							else if ((r == 5) && player.ReturnVie() < 3) {

								Matrice[y + i][x] = '&'; //case avec bonus +vie
								b = false;

							}
							else {
								Matrice[y + i][x] = '0';  //case vide
								b = false;
							}
						}
					}

				}
				/*-----------------*/
				/*--HAUT--*/
				if (y - i > 0) {
					murH = false;
					if (Matrice[y - i][x] == '6') { //haut
						Matrice[y - i][x] = '0'; //case vide
					}
					else if (Matrice[y - i][x] == '7' && h == false) {
						Matrice[y - i][x] = '0'; //case vide
					}
					else if (Matrice[y - i][x] == '7' && h == true) {
						if (y - i == level.keyY && x == level.keyX) {
							Matrice[y - i][x] = '@';  //case avec un bonus +nbBombe
							h = false;
						}
						else {
							srand(time(NULL));
							int r = rand() % 10 + 1;  //valeur al�atoire pour mettre un bonus ou non 
							cout << "haut : " << r << "\n";
							if (r == 1) {
								Matrice[y - i][x] = '0';  //case vide
								h = false;

							}
							else if (r == 2) {


								Matrice[y - i][x] = '+';  //case avec un bonus +nbBombe
								h = false;

							}
							else if (r == 3) {

								Matrice[y - i][x] = '*'; //case avec un bonus +vitesse
								h = false;

							}
							else if (r == 4) {

								Matrice[y - i][x] = '/'; //case avec un bonus +distance
								h = false;

							}
							else if ((r == 5) && player.ReturnVie() < 3) {

								Matrice[y - i][x] = '&'; //case avec un bonus +vie
								h = false;

							}
							else {
								Matrice[y - i][x] = '0';  //case vide
								h = false;
							}
						}
						
					}

				}

			}

			
		}
	
}



int Bombe::TimerBombe() {

	return startTime;	
	
}

void Bombe::TimerExplose() {

	explose = true;
	timer = true;
	Destroy();
	glutTimerFunc(1000, timerBombeExplose, 0);
	
}

void Bombe::UpdateDistance(int _distance)
{
	distance = _distance;
}


bool Bombe::ExploseDroite() {

	for (int i = 1; i <= distance; i++) {

		if (Matrice[y][x + i] == '1' || Matrice[y][x + i] == '|' || Matrice[y][x + i] == '@' || Matrice[y][x + i] == '~' || Matrice[y][x + i] == '~') return false;
		else return true;
	}
}
bool Bombe::ExploseGauche() {

	for (int i = 1; i <= distance; i++) {

		if (Matrice[y][x - i] == '1' || Matrice[y][x - i] == '|' || Matrice[y][x - i] == '@' || Matrice[y][x - i] == '~' || Matrice[y][x - i] == '�') return false;
		else return true;
	}

}
bool Bombe::ExploseHaut() {

	for (int i = 1; i <= distance; i++) {

		if (Matrice[y-i][x] == '1' || Matrice[y-i][x] == '|' || Matrice[y-i][x] == '@' || Matrice[y - i][x] == '~' || Matrice[y - i][x] == '�') return false;
		else return true;
	}
}
bool Bombe::ExploseBas() {

	for (int i = 1; i <= distance; i++) {

		if (Matrice[y + i][x] == '1' || Matrice[y + i][x] == '|' || Matrice[y + i][x] == '@' || Matrice[y + i][x] == '~' || Matrice[y + i][x] == '�') return false;
		else return true;
	}
}

bool Bombe::TouchDroite() {

	for (int i = 1; i <= distance; i++) {

		if (Matrice[y][x + i] == '#') return true;
		else return false;
	}
}
bool Bombe::TouchGauche() {

	for (int i = 1; i <= distance; i++) {

		if (Matrice[y][x - i] == '#') return true;
		else return false;
	}

}
bool Bombe::TouchHaut() {

	for (int i = 1; i <= distance; i++) {

		if (Matrice[y - i][x] == '#') return true;
		else return false;
	}
}
bool Bombe::TouchBas() {

	for (int i = 1; i <= distance; i++) {

		if (Matrice[y + i][x] == '#') return true;
		else return false;
	}
}






