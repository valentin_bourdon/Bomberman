/////////////////////////////////////////////////////////////
//														   //
//				PROJET BOMBERMAN 						   //
//														   //
//				----- Class Avatar -----				   //
//														   //
/////////////////////////////////////////////////////////////

#include "avatar.h"
#include "Niveau.h"
#include "Bonus.h"
#include "Bombe.h"

using namespace std;

extern char** Matrice;

extern int NbColonnes;
extern int NbLignes;
extern Bonus bonnus;
extern int tailleTableauBombe;

extern bool touch;
extern Niveau level;

extern vector<GLuint> texture; 
extern vector<Bombe> bombe;


avatar::avatar(int abs, int ord, int img)
{
	x = abs;
	y = ord;
	offsetX = 0.0;
	offsetY = 0.0;
	nbBombe = 1;
	vitesse = 0.1;
	_img = img;
	nbVies = 3;
	touch = false;
	depH = false;
	depB = false;
	depG = false;
	depD = false;
	key = false;
		
}

int avatar::ReturnVitesse()
{
	return vitesse;
}

int avatar::NbBombe()
{
	return nbBombe;
}



//POSITION//

int avatar::PositionX()
{
	return x;
}

int avatar::PositionY()
{
	return y;
}

void avatar::ResetPos(int abs, int ord)
{
	x = abs;
	y = ord;
}


// Fonctions de déplacements de l'avatar //

void avatar::DepGauche()
{
	depH = false;
	depB = false;
	depG = true;
	depD = false;

	/*Gestion des textures*/
	if (level.niveau == 1) {
		if (touch == true)_img = 75;
		else _img = 2;
	}
	else if (level.niveau == 2) {
		if (touch == true)_img = 78;
		else _img = 6;
	}
	else if (level.niveau == 3) {
		if (touch == true)_img = 84;
		else _img = 10;
	}
	
	

	/*Gestion du déplacement*/
	if (((Matrice[y][x - 1] =='0') || (Matrice[y][x - 1] =='+') || (Matrice[y][x - 1] == '*') || (Matrice[y][x - 1] == '/') ||  (Matrice[y][x - 1] == '&') || (Matrice[y][x - 1] == '@') || (Matrice[y][x - 1] == '|') ) &&
		(x > 0)) {
		offsetX -= vitesse;
		offsetY = 0;
		if (offsetX < -0.90) {
			offsetX = 0.0;
			x--;
		}

		//Gestion des Bonus//
		if (Matrice[y][x] == '+') {
			Matrice[y][x] = '0';
			bonnus.UpdateNbBombe();
		}
		if (Matrice[y][x] == '*') {
			Matrice[y][x] = '0';
			bonnus.UpdateVitesse();
		}
		if (Matrice[y][x] == '/') {
			Matrice[y][x] = '0';
			bonnus.UpdateDistance();
		}
		if (Matrice[y][x] == '&') {
			Matrice[y][x] = '0';
			bonnus.UpdateDistance();
		}
		if (Matrice[y][x] == '@') {
			Matrice[y][x] = '0';
			key = true;
		}

		
	}
	else if (((Matrice[y][x - 1] == '1') || (Matrice[y][x - 1] == '2') || (Matrice[y][x - 1] == '+') || (Matrice[y][x - 1] == '*') || (Matrice[y][x - 1] == '/') || (Matrice[y][x - 1] == '&') || (Matrice[y][x - 1] == '@') || (Matrice[y][x - 1] == '|') || (Matrice[y][x - 1] == '#') || (Matrice[y][x - 1] == '°')) && (offsetX > 0.1) && (offsetY<0.1 && offsetY>-0.1)) {
		offsetX -= vitesse;
		offsetY = 0;
		if (offsetX < -0.90) {
			offsetX = 0.0;
			x--;
		}

		//Gestion des Bonus//
		if (Matrice[y][x] == '+') {
			Matrice[y][x] = '0';
			bonnus.UpdateNbBombe();
		}
		if (Matrice[y][x] == '*') {
			Matrice[y][x] = '0';
			
			bonnus.UpdateVitesse();
		}
		if (Matrice[y][x] == '/') {
			Matrice[y][x] = '0';
			bonnus.UpdateDistance();
		}
		if (Matrice[y][x] == '&') {
			Matrice[y][x] = '0';
			bonnus.UpdateVie();
		}
		if (Matrice[y][x] == '@') {
			Matrice[y][x] = '0';
			key = true;
		}

		
	}

}

void avatar::DepDroite()
{
	depH = false;
	depB = false;
	depG = false;
	depD = true;

	/*Gestion des textures*/
	if (level.niveau == 1) {
		if (touch == true)_img = 76;
		else _img = 3;
	}
	else if (level.niveau == 2) {
		if (touch == true)_img = 79;
		else _img = 7;
	}
	else if (level.niveau == 3) {
		if (touch == true)_img = 83;
		else _img = 11;
	}
	
		
	/*Gestion du déplacement*/
	if (((Matrice[y][x + 1] =='0') || (Matrice[y][x + 1] =='+') || (Matrice[y][x + 1] == '*') || (Matrice[y][x + 1] == '/') || (Matrice[y][x + 1] == '&') || (Matrice[y][x + 1] == '@') || (Matrice[y][x + 1] == '|') ) &&
		(x <= 11) ) {
		offsetX += vitesse;
		offsetY = 0;
		if (offsetX > 0.90) {
			offsetX = 0.0;
			x++;
		}
		//Gestion des Bonus//
		if (Matrice[y][x] == '+') {
			Matrice[y][x] = '0';
			bonnus.UpdateNbBombe();
			
		}
		if (Matrice[y][x] == '*') {
			Matrice[y][x] = '0';
			bonnus.UpdateVitesse();
		}
		if (Matrice[y][x] == '/') {
			Matrice[y][x] = '0';
			bonnus.UpdateDistance();
		}
		if (Matrice[y][x] == '&') {
			Matrice[y][x] = '0';
			bonnus.UpdateVie();
		}
		if (Matrice[y][x] == '@') {
			Matrice[y][x] = '0';
			key = true;
		}

		
	}
	else if (((Matrice[y][x + 1] == '1')|| (Matrice[y][x + 1] == '2') || (Matrice[y][x + 1] == '+') || (Matrice[y][x + 1] == '*') || (Matrice[y][x + 1] == '/') || (Matrice[y][x + 1] == '&') || (Matrice[y][x + 1] == '@') || (Matrice[y][x + 1] == '|') || (Matrice[y][x + 1] == '#') || (Matrice[y][x + 1] == '°') || (Matrice[y][x + 1] == '~')) && (offsetX < -0.1) && (offsetY<0.1 && offsetY>-0.1)) {
		offsetX += vitesse;
		offsetY = 0;
		if (offsetX > 0.90) {
			offsetX = 0.0;
			x++;
		}
		//Gestion des Bonus//
		if (Matrice[y][x] == '+') {
			Matrice[y][x] = '0';
			bonnus.UpdateNbBombe();
			
		}
		if (Matrice[y][x] == '*') {
			Matrice[y][x] = '0';
			bonnus.UpdateVitesse();
		}
		if (Matrice[y][x] == '/') {
			Matrice[y][x] = '0';
			bonnus.UpdateDistance();
			
		}
		if (Matrice[y][x] == '&') {
			Matrice[y][x] = '0';
			bonnus.UpdateVie();
		}
		if (Matrice[y][x] == '@') {
			Matrice[y][x] = '0';
			key = true;
		}

		
	}

}

void avatar::DepTop()
{
	depH = true;
	depB = false;
	depG = false;
	depD = false;

	/*Gestion des textures*/
	if (level.niveau == 1) {
		if (touch == true)_img = 77;
		else _img = 4;
	}
	else if (level.niveau == 2) {
		if (touch == true)_img = 80;
		else _img = 8;
	}
	else if (level.niveau == 3) {
		if (touch == true)_img = 84;
		else _img = 12;
	}
	
	/*Gestion du déplacement*/
	if (((Matrice[y - 1][x] =='0') || (Matrice[y - 1][x] =='+') || (Matrice[y - 1][x] == '*') || (Matrice[y - 1][x] == '/') || (Matrice[y - 1][x] == '&') || (Matrice[y - 1][x] == '@') || (Matrice[y - 1][x] == '|')) &&
		(y > 0)) {
		offsetY -= vitesse;
		offsetX = 0;
		if (offsetY < -0.90) {
			offsetY = 0.0;
			y--;
		}

		//Gestion des Bonus//
		if (Matrice[y][x] == '+') {
			Matrice[y][x] = '0';
			bonnus.UpdateNbBombe();
			
		}
		if (Matrice[y][x] == '*') {
			Matrice[y][x] = '0';
			bonnus.UpdateVitesse();
		}
		if (Matrice[y][x] == '/') {
			Matrice[y][x] = '0';
			bonnus.UpdateDistance();
		}
		if (Matrice[y][x] == '&') {
			Matrice[y][x] = '0';
			bonnus.UpdateVie();
		}
		if (Matrice[y][x] == '@') {
			Matrice[y][x] = '0';
			key=true;
		}

		
	}
	else if(((Matrice[y - 1][x] == '1') || (Matrice[y - 1][x] == '2') || (Matrice[y - 1][x] == '+') || (Matrice[y - 1][x] == '*') || (Matrice[y - 1][x] == '/') || (Matrice[y - 1][x] == '&') || (Matrice[y - 1][x] == '@') || (Matrice[y - 1][x] == '|') || (Matrice[y - 1][x] == '#') || (Matrice[y - 1][x] == '°') || (Matrice[y - 1][x] == '~')) && (offsetY > 0.1) ) {
		offsetY -= vitesse;
		offsetX = 0;
		if (offsetY < -0.90) {
			offsetY = 0.0;
			y--;
		}

		//Gestion des Bonus//
		if (Matrice[y][x] == '+') {
			Matrice[y][x] = '0';
			bonnus.UpdateNbBombe();
			
		}
		if (Matrice[y][x] == '*') {
			Matrice[y][x] = '0';
			bonnus.UpdateVitesse();
		}
		if (Matrice[y][x] == '/') {
			Matrice[y][x] = '0';
			bonnus.UpdateDistance();
			
		}
		if (Matrice[y][x] == '&') {
			Matrice[y][x] = '0';
			bonnus.UpdateVie();
		}
		if (Matrice[y][x] == '@') {
			Matrice[y][x] = '0';
			key = true;
		}

		
	}
	
}

void avatar::DepBottom()
{
	depH = false;
	depB = true;
	depG = false;
	depD = false;

	/*Gestion des textures*/
	if (level.niveau == 1) {
		if (touch == true)_img = 75;
		else _img = 5;
	}
	else if (level.niveau == 2) {
		if (touch == true)_img = 81;
		else _img = 9;
	}
	else if (level.niveau == 3) {
		if (touch == true)_img = 82;
		else _img = 13;
	}
	
	/*Gestion du déplacement*/
	if(((Matrice[y + 1][x] =='0') || (Matrice[y + 1][x] =='+') || (Matrice[y + 1][x] == '*') || (Matrice[y + 1][x] == '/') || (Matrice[y + 1][x] == '&') || (Matrice[y + 1][x] == '@') || (Matrice[y + 1][x] == '|') ) &&
		(y <= 11)) {
		
			offsetY += vitesse;
			offsetX = 0;
			if (offsetY > 0.90) {
				offsetY = 0.0;
				y++;
			}
			//Gestion des Bonus//
			if (Matrice[y][x] == '+') {
				Matrice[y][x] = '0';
				bonnus.UpdateNbBombe();
				
			}
			if (Matrice[y][x] == '*') {
				Matrice[y][x] = '0';
				
				bonnus.UpdateVitesse();
			}
			if (Matrice[y][x] == '/') {
				Matrice[y][x] = '0';
				bonnus.UpdateDistance();
				
			}
			if (Matrice[y][x] == '&') {
				Matrice[y][x] = '0';
				bonnus.UpdateVie();
			}
			if (Matrice[y][x] == '@') {
				Matrice[y][x] = '0';
				key = true;
			}

			

	}
	else if (((Matrice[y + 1][x] == '1') || (Matrice[y + 1][x] == '2') || (Matrice[y + 1][x] == '+') || (Matrice[y + 1][x] == '*') || (Matrice[y + 1][x] == '/') || (Matrice[y + 1][x] == '&') || (Matrice[y + 1][x] == '@') || (Matrice[y + 1][x] == '|') || (Matrice[y + 1][x] == '#') || (Matrice[y + 1][x] == '°') || (Matrice[y + 1][x] == '~')) && (offsetY < -0.1) /*&& (offsetX<0.1 && offsetX>-0.1)*/) {
		offsetY += vitesse;
		offsetX = 0;
		if (offsetY > 0.90) {
			offsetY = 0.0;
			y++;
		}
		//Gestion des Bonus//
		if (Matrice[y][x] == '+') {
			Matrice[y][x] = '0';
			bonnus.UpdateNbBombe();
		}
		if (Matrice[y][x] == '*') {
			Matrice[y][x] = '0';
			bonnus.UpdateVitesse();
		}
		if (Matrice[y][x] == '/') {
			Matrice[y][x] = '0';
			bonnus.UpdateDistance();
		}
		if (Matrice[y][x] == '&') {
			Matrice[y][x] = '0';
			bonnus.UpdateVie();
		}
		if (Matrice[y][x] == '@') {
			Matrice[y][x] = '0';
			key = true;
		}
	}
	
}


//Fonction pour afficher notre avatar. Nous utilisons une texture pour le représenter
void avatar::Dessiner()
{
	if (level.niveau == 1) {
		if (touch == true && (depB == true || depG == true))_img = 76;
		else if (touch == true && depD == true)_img = 75;
		else if (touch == true && depH == true)_img = 77;
	}
	else if (level.niveau==2) {
		if (touch == true && depB == true)_img = 81;
		else if (touch == true && depD == true)_img = 79;
		else if (touch == true && depH == true)_img = 80;
		else if (touch == true && depG == true)_img = 78;
	}
	else if (level.niveau==3) {
		if (touch == true && (depB == true || depG == true))_img = 83;
		else if (touch == true && depD == true)_img = 82;
		else if (touch == true && depH == true)_img = 84;
	}
	
	glPushMatrix();

	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glBindTexture(GL_TEXTURE_2D, texture[_img]);
	glBegin(GL_QUADS);
	glColor4d(1.0, 1.0, 1.0, 1.0);
	glTexCoord2f(1.0f, 1.0f); glVertex2d(x + offsetX, y + offsetY);
	glTexCoord2f(0.0f, 1.0f); glVertex2d(x + 1 + offsetX, y + offsetY);
	glTexCoord2f(0.0f, 0.0f); glVertex2d(x + 1 + offsetX, y + 1 + offsetY);
	glTexCoord2f(1.0f, 0.0f); glVertex2d(x + offsetX, y + 1 + offsetY);
	glEnd();
	glDisable(GL_TEXTURE_2D);
	glPopMatrix();
	glutPostRedisplay();
	
}
bool avatar::ReturnKey()
{
	return key;
}

//VIE//
void avatar::UpdateVie()
{
	
	nbVies--;
}
int avatar::ReturnVie()
{
	return nbVies;
}
void avatar::Reset(int abs,int ord,int img)
{
	nbVies = 3;
	offsetX = 0.0;
	offsetY = 0.0;
	nbBombe = 1;
	vitesse = 0.1;
	key = false;
	x = abs;
	y = ord;
	_img = img;
	
}

