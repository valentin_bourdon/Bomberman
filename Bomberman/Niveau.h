/////////////////////////////////////////////////////////////
//														   //
//		/*PROJET BOMBERMAN - MTI 3D - */				   //
//														   //
//				----- Class Niveau -----				   //
//														   //
/////////////////////////////////////////////////////////////
#pragma once
#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>
#include <windows.h> 
#include "SOIL/SOIL.h"
#include "GL\glut.h"

class Niveau
{


public:
	
	int NbLignes; 
	int NbColonnes; 
	int niveau; // Varibale pour stocker le num�ro du niveau courant
	int img;
	
	int keyX; //Position X de la cl�
	int keyY; // Position Y de la cl�
	
	Niveau();

	void OuvrirNiveau(char*);
	void DessinerNiveau();
	void Key();

	void Texture(int);
	
};

