/////////////////////////////////////////////////////////////
//														   //
//		/*PROJET BOMBERMAN - MTI 3D - */				   //
//														   //
//				----- Class Bombe -----				       //
//														   //
/////////////////////////////////////////////////////////////

#pragma once

#include <iostream>
#include <vector>
#include <windows.h> 
#include "SOIL/SOIL.h"
#include "GL\glut.h"
#include <time.h>



class Bombe
{

	//static Bombe *instance;
	

public: 
	
	
	int x;
	int y;
	int distance;
	int img;

	bool explose;
	bool pose;

	bool d; //variable pour savoir s'il y a un mur destructible � droite (dans le champ d'explosion de la bombe)
	bool g; //variable pour savoir s'il y a un mur destructible � gauche (dans le champ d'explosion de la bombe)
	bool h; //variable pour savoir s'il y a un mur destructible en haut (dans le champ d'explosion de la bombe)
	bool b; //variable pour savoir s'il y a un mur destructible en bas (dans le champ d'explosion de la bombe)

	bool murD;
	bool murG;
	bool murB;
	bool murH;

	bool timer;

	int startTime;

	bool touchBombe;
	
	

public:

	Bombe();

	void UpdatePosition(int, int);

	int PositionX();
	int PositionY();

	
	void Destroy();


	bool ExploseDroite();
	bool ExploseGauche();
	bool ExploseHaut();
	bool ExploseBas();

	bool TouchDroite();
	bool TouchGauche();
	bool TouchHaut();
	bool TouchBas();

	int TimerBombe();
	void TimerExplose();
	

	void UpdateDistance(int);
	
	
};

