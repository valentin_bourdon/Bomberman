# Bomberman

Bomberman developed in C++ and OpenGL.

# Requirements

- Visual Studio

# Installation

:one: Clone this repository

:two: Open the visual studio project `Bomberman.sln`

:three: Add dependencies:
- `Project > Properties > C/C++ > General > Additional Include Directories`
	- Add **Bomberman/GL** folder path
    - Add **Bomberman/FMOD** folder path
    - Add **Bomberman/SOIL** folder path
- `Project > Properties > Linkers > Input`
	- Add in `Additional dependencies` : **glut32.lib**,**SOIL.lib**,**fmodex_vc.lib** and **SDL.lib**
- `Project > Properties > Linkers > General`
	- Add in `Additional Library Directories` : **Bomberman/LIB** folder path
- `C:\Windows\System32`
	- Copy/Paste **glut32.dll** and **fmodex.dll**
	
:four: Run
- Release - x86

# License

All the changes made are released under the MIT License, see [LICENSE](./LICENSE).
